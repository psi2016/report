import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import cm 

# Author: Matthias Oscity

x2 = np.array([1800,1900,2000,2100,2200])
y2 = np.array([226828,409728,486226,664479,1332559])

x3 = np.array([1850,1900,1950,2000,2050,2100,2150])
y3 = np.array([237297,343535,459936,533856,556796,567195,571388])

x4 = np.array([1800,1900,2000,2100,2200])
y4 = np.array([7944,161392,274338,492217,579579])

x5 = np.array([2000,2050,2100,2150,2200]) 
y5 = np.array([356479,402220,408660,542981,1184341])

x11 = np.array([1950,2000,2050,2100,2150,2200])
y11 = np.array([43188,247188,426507,549279,728244,1243110.1]) # Last entry too large without comma?

x13 = np.array([1800,1900,2000,2100,2200])
y13 = np.array([203506,406789,459491,524945,859280])

x14 = np.array([2000,2050,2100,2150,2200,2250,2300,2350,2400,2450,2500])
y14 = np.array([18038,73972,149299,225323,293021,349494,389859,427004,457938,488895,518989])

x15 = np.array([1850,1900,1950,2000,2050,2100])
y15 = np.array([219632,279911,305325,316341,341221,451091])

x16 = np.array([1600,1650,1700,1750,1800,1850])
y16 = np.array([243521,353573,410502,440109,465027,510991])

x17 = np.array([1950,2000,2050,2100,2150,2200,2250,2300])
y17 = np.array([220126,300315,361335,396579,416825,435990,473338,503698])

x18 = np.array([1850,1900,1950,2000,2050,2100,2150,2200,2250,2300,2350,2400])
y18 = np.array([130021,259338,371446,463882,536053,613241,681836,752919,822107,886768,983599,1124731.2]) # Last entry too large without comma?

x19 = np.array([2000,2050,2100,2150,2200,2250])
y19 = np.array([543357,569561,593528,637763,764004,1133485])

x20 = np.array([1650,1750,1850,1900,1950,2050])
y20 = np.array([255390,299869,345300,401651,611233,2264328])

x22 = np.array([2000,2050,2100,2150,2200,2250,2300])
y22 = np.array([120210,220159,281326,337099,467973,675376,893497])


x = [x2,x3,x4,x5,x11,x13,x14,x15,x16,x17,x18,x19,x20,x22]
y = [y2,y3,y4,y5,y11,y13,y14,y15,y16,y17,y18,y19,y20,y22]
label_index = [2,3,4,5,11,13,14,15,16,17,18,19,20,22]


# Plots
print '--- Plotting Data ---'
PlotWidth = 10
PlotHeight = 7


## Plot Singles ##
print '- Single Plots'
for j in range(len(x)):
    plt.figure(figsize=(PlotWidth,PlotHeight))
    # Errorbars with 5V Error in  x and 4kHz Error in y
    plt.errorbar(x[j],y[j]/1000.0,xerr=5,yerr=4,color='k',fmt='-o',label='S' + str(label_index[j]))
    plt.title('Plateau characteristic of scintillator Nr.' + str(label_index[j]),y=1.05,fontsize=20)
    plt.xlabel('Supply Voltage [V]',fontsize=16)
    plt.ylabel('Count Rate [kHz]',fontsize=16)
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
    plt.grid(True)
    plt.savefig('./s' + str(label_index[j]) + '.pdf',bbox_inches='tight')
    plt.close()
   
    
## Plot All ##
print '- All In One Plot'
color=iter(cm.rainbow(np.linspace(0,1,len(x))))

plt.figure(figsize=(PlotWidth,PlotHeight))
for j in range(len(x)):
    c=next(color)
    # Errorbars with 5V Error in  x and 4kHz Error in y
    plt.errorbar(x[j],y[j]/1000.0,xerr=5,yerr=4,c=c,fmt='-o',label='S' + str(label_index[j]))
plt.title('Plateau characteristics of all scintillators',y=1.05,fontsize=20)
plt.xlabel('Supply Voltage [V]',fontsize=16)
plt.ylabel('Count Rate [kHz]',fontsize=16)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
plt.grid(True)
plt.savefig('./S_All.pdf',bbox_inches='tight')
plt.close()

## Plot Used ##
print '- All Used In One Plot'
x_used = [x4,x5,x13,x14,x16,x22]
y_used = [y4,y5,y13,y14,y16,y22]
label_index_us = [4,5,13,14,16,22]
label_index_used = ['4 = e_sci2','5 = b_sci4','13 = e_sci1','14 = y_veto','16 = b_sci3','22 = b_sci2']
HV_used = [2000,2100,2000,2350,2100,2100]

color=iter(cm.rainbow(np.linspace(0,1,len(x_used))))

plt.figure(figsize=(PlotWidth,PlotHeight))
for j in range(len(x_used)):
    c=next(color)
    # Errorbars with 5V Error in  x and 4kHz Error in y
    plt.errorbar(x_used[j],y_used[j]/1000.0,xerr=5,yerr=4,c=c,fmt='-o',label='S' + label_index_used[j])
    plt.bar(HV_used[j],1200,1,bottom=0,color=c,label='S' + str(label_index_us[j]) + ': HV = ' + str(HV_used[j]/1000.0) + 'kV' )
plt.title('Plateau characteristics of used scintillators',y=1.05,fontsize=20)
plt.xlabel('Supply Voltage [V]',fontsize=16)
plt.ylabel('Count Rate [kHz]',fontsize=16)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
plt.grid(True)
plt.savefig('./S_used.pdf',bbox_inches='tight')
plt.close()

print '----------------------'
print 'Done.'
