The report tex files are compiling now!

Please push a new version only if it's compiling as well...

This is how it works:
main_report.tex is the main file in which we include all chapter etc. using 
\include{myNewChapter}

myNewChapter.tex:
1st line is \chapter{myNewChapter}
then comes the text, figures etc.

It's much easier to merge the work of multiple people later when we write only one sencence per line (in the editor).

IMPORTANT:
Before pushing a new version, type:
touch .gitignore

This creates a file called ".gitignore". Then copy all file names of .aux files and so on in there.
This prevents these files from being pushed into the repository. We need only the .tex files there (and bib.bib).


Have fun!

UPDATE:
How to add references...
I'm using JabRef (free software) to add stuff to the bib file. That makes it pretty easy but you can edit the bib file as well
if you want.
In the text you can use \citep{whateveryourbibtexkeyis}.

When it comes to compiling and you're wondering about question marks that don't go away, do this:
1. compile PDFLaTeX
2. compile BibTeX
3. compile PDFLaTeX TWICE!

That should work...it's a weird feature of LaTeX that you have to compile several times!