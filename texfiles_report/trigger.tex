\chapter{Trigger}
\label{chap:trigger}

The signals arriving from the detectors and calorimeters are combined with logic NIM units, such that only when certain events happen a trigger is activated and data is read out and saved. 
The trigger logic consists of roughly three parts, each corresponding to another arrangement of detectors and calorimeters in our experiment: the beam monitor, the electron detection arm and the photon detection arm. 

First, the final ``read out'' trigger was planned to be an AND combination of all three parts, but the event rate turned out to be very low (below \SI{1}{\hertz}). 
Therefore, throughout the experiment different combinations of these parts were set as the final trigger. 
In the next section, the three parts are described in more detail. \\ \\
Before the detector responses can be combined with logic NIM units, the signals need to be discriminated and delayed. 
This means that the raw signals are converted into binary levels with adjustable width, when a certain threshold is exceeded. 
Both parameters, threshold and width, can be adjusted manually on each discriminator unit. 
Additionally, the signals have to arrive at the same time in the logic unit (on a nanosecond scale), which is achieved by using delay units and adapting cable lengths.

\section{The Beam Monitor}
\label{section:beam_monitor}
The beam monitor trigger logic evaluates whether a charged particle is stopped in the target or not. 
For this purpose, three scintillation detectors (\texttt{b\_sci1}, \texttt{b\_sci2}, \texttt{b\_sci3}) before the target and one with a large area behind the target are combined as shown in the blue box in figure \ref{fig:trigger_logic}. 
The first two scintillation detectors after the beam opening are combined with a logic AND to ensure an incoming particle from the beamline. 
After the moderators another scintillation detector (\texttt{b\_sci3}) registers whether the particles passed through the moderators. 
This third detector is combined with the signal of the first two with an AND. 
The signal of the last detector behind the target is inverted. Therefore, it triggers when no particle hits this scintillator, and then feeds the signal into the final AND unit to create the \texttt{$\pi$-\textit{stop}-trigger}.

\section{The Electron Arm}
The two scintillation detectors (\texttt{e\_sci1}, \texttt{e\_sci2}) on the electron arm side are combined with a logic AND unit to ensure that charged particles are being produced or passing through the copper plate. This is already the final trigger (\texttt{$e$-trigger}) from this section. The small calorimeter behind these two detectors acts as an additional energy reference, which is fed into the DAQ separately. It does not play any role in the trigger scheme.

\section{The Photon Arm}

On the photon side, a scintillator (\texttt{$\gamma$\_sci})  acts as a veto. Behind this scintillator, there is a large calorimeter consisting of a large volume of scintillating crystal (NaI) read out by six PMTs. The discriminated signal of the scintillator is combined with the summed signal of the six PMTs with an AND unit (and discriminated again) to create the \texttt{$\gamma$-trigger}. Each PMT signal as well as the summed signal are fed into the DAQ. \\ \\

\begin{table}[H]
\centering
\begin{tabular}{lrrr }
\toprule
\bfseries Name & \bfseries Label & \bfseries Position & \bfseries Purpose - Check ...  \\
\midrule
b\_sci1 & S1 & $1^{\textnormal{st}}$ after beam opening & if beam is on \\
b\_sci2 & S22 & $2^{\textnormal{nd}}$ after beam opening & if charged particles pass \\
b\_sci3 & S16 & after moderators  & if particles pass moderators \\
b\_sci4 & S5 & behind target & if particles stop in target \\
e\_sci1 & S13 & $1^{\textnormal{st}}$ detector & if charged particles arrived \\
e\_sci2 & S4 & $2^{\textnormal{nd}}$ detector & if charged particles passed\\
e\_calo & Small Calo & after detectors  & particles energy \\
y\_veto & S14 & tube opening & if uncharged particles pass \\
y\_calos & Big Calo & inside tube & particles energy \\
\bottomrule
\end{tabular}
\caption{Trigger Logic Components.}
\label{tabular:components}
\end{table}

\begin{figure}[h]
	\centering
	\includegraphics[width=1.5\textwidth, angle = 90]{trigger_logic.pdf}
	\caption{Schematic of the trigger logic.}
	\label{fig:trigger_logic}
\end{figure}
