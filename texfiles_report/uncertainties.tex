
\chapter{Uncertainties}
\label{chapter:uncertainties}


In the following we want to discuss the main sources of uncertainties in our experimental setup. 
We will not discuss statistical uncertainties due to the lack of signal events.
The extent of the systematic uncertainties might be a reason for the lack signal events, in particular the use of wrong firmware on the logicboxes, as discussed below.

\section{Systematic uncertainties}
Numerous systematic uncertainties contribute to the final deviation of our measurement from the expectations.
\subsection{Pion Beam}

\paragraph{Momentum fluctuations}
The pion beam provided in the $\pi$M1 beamline at PSI is subject to statistical fluctuations in momentum and spot size on the target. 
A change in the beam energy in our case results in a shift of pion stopping rate. 
The momentum resolution of the beamline is \SI{0.1}{\percent}, so at a pion momentum of \SI{176}{\mega \electronvolt\per c} this results in an uncertainty of \SI{1.76}{\mega \electronvolt\per c} \cite{piM1}.

Comparing this to the range curves in section \ref{section:range_curve} we can conclude that the momentum uncertainty does not significantly shift the pion stopping rate from the maximum and thus does not influence the results within the scope of our accuracy. 
Furthermore, we would measure a ratio, so we would normalize the number of found events. 
This would cancel the stopping range uncertainty.%\textcolor{red}{Nik: uncertainty != jumps} \textcolor{green}{What does that mean?}


\paragraph{Spot size}
The spot size of $10\times$\SI{10}{\milli\meter} principally causes a loss of acceptance of the experimental setup, as it was constructed with a point like beam in mind. 
Additionally, this uncertainty can cause deviations in the conversion probability within the target. Thus, it is very difficult to precisely compute a value for $C_{\text{plastic}}$. 

In our calculations of the conversion probability we used the expectation value, which is half the thickness of the target. 
Taking into consideration that the beam and the target are not point-like could lead to larger deviations.

\paragraph{Contamination with electrons and muons}
The beam which is provided in $\pi$M1 is constituted of pions, electrons and muons. 
This could cause fake signal-like events if they are stopped in the target or not detected by the beam veto scintillator behind the target if they fly through without stopping.

In figure \ref{fig:beam_composition}, we can read of that at a momentum of \SI{176}{\mega \electronvolt\per c} the electron flux is approximately at the same level as the pion flux. 
However, the probability of an electron to emit a photon pair within the target or to be deflected and to radiate off two photons within the solid angle we cover is negligible.

For muon fluxes there is no data available at \SI{176}{\mega \electronvolt\per c}, but at \SI{300}{\mega \electronvolt\per c} the muon flux is at \SI{1}{\percent} of the pion flux and falls of more slowly towards low \mbox{momenta \citep{pi_el_flux}}. 
This means that the muon flux is expected to be slightly higher than the pion flux at our energy. 
Most muons transverse the target without being stopped and thus trigger the beam veto. 
For other cases we again neglected the probability that a muon decay combined with initial-state-radiation or final-state-radiation occurs within our energy range and our acceptance.
 
Both these cases would, if they were falsely identified as pion decay, increase the signal we see, which cannot be concluded from the spectrum we measured.

\subsection{Mechanical setup}

\paragraph{Geometric alignment}
%\textcolor{red}{Should we not show how we actually find these errors?}\\
%\textcolor{blue}{Julian: What do you mean? The maximum precision of measuring distances etc.?} \textcolor{red}{Nicole: For both the 60\% and the 26 \% these were not just estimates, right? We have uncertainties for target \& moderator $\rightarrow$ relative error for solide angle. It might be a good idea to insert some formula to say how we actually got to that error. Might also be overdoing it though}
% Julian: I think we did that in the respective chapter, so it wouldn't be needed here
%Nicole: found it, sorry, was confused

The alignment of the detectors, the moderators and the target is also subject to uncertainties leading to a relative error of the solid angle of about \SI{60}. %{\percent} \textcolor{green}{Where does this number come from? reference to that section}
If we normalized our results, this uncertainty would cancel. The measurement precision of the thicknesses of the material in the electron arm together with the acceptance limits the relative uncertainty to \SI{26}{\percent} in the worst case. 

For the calculation of the expected rate it was crucial to specify the thicknesses of different materials in the electron arm as precisely as possible for a sensible prediction. 
In case of the thickness of the tape and the aluminium foil guesses to best knowledge had to be used.
But even with the most conservative assumption, the lack of signal events cannot be explained. 
Still, there are contributions of these uncertainties in the final result.


\paragraph{Detector efficiencies}

In general, detector efficiencies apart from the defect of one PMT in the calorimeter should not significantly influence the data taking process. 
In the worst case, fewer events are observed than expected, which is not a significant contribution (and would vanish if we normalized the result) if the scintillators are supplied by a voltage which lies in the operating region of each scintillator. 

However, there was one scintillator, S16 in figure \ref{fig:ScintillatorEfficiencies}, which was mistakenly operated at \SI{2.1}{\kilo \volt} even though its operating region seems to be between \SI{1.6}{\kilo \volt} and \SI{1.8}{\kilo \volt}.
But since we have no data for higher voltages for this scintillator, we cannot conclude with certainty that the plateau region is not also at around \SI{2.1}{\kilo \volt}.

As this scintillator was in the trigger system of the beam monitor (see figure \ref{fig:trigger_logic}, \texttt{b\_sci3}), it was part of the \texttt{$\pi$-stop trigger}. 
Thus, a high voltage applied for operation could strongly influence the final trigger output to a higher count rate. 
The range curve measurements would then show a slightly higher stop rate, but since the extracted values for the optimal thickness match together with the expected ranges (see \ref{section:range_curve}), this error can also most likely be neglected.

\paragraph{Unexpected incidents}
During the experiment, the target, which was carefully mounted with strings to be held in place, fell down and thus led to a number of runs which were unusable. 

Remounting the target led to further uncertainties in the geometric alignment. 
As discussed before, the geometric alignment does contribute to uncertainties in the final result, but the lack of events cannot be affiliated to this incident, since the remounting was executed carefully and the affected data were rejected.\\ 

While calibrating the calorimeter during the mechanical setup, one PMT broke due to high voltage. 
The PMTs were arranged in circle of six around one central PMT. The broken PMT was part of the circle. This results in a reduction of the amplitude of the readout signal in the partially defect calorimeter.

This contributes to the overall uncertainty of the experiment, because it cannot be formulated quantitatively without performing simulations and a sophisticated the analysis, especially the extraction of a distinct signal as the spectrum was washed out.

\subsection{DAQ}
\paragraph{LabVIEW}
The LabVIEW program which was created for this experiment was subject to frequent crashes. 
These crashes did not follow a specific pattern but happened incidentally. 
As a consequence, some data files were corrupted during the crash and thus the last fragments of data within this run was not usable. 
Additionally, the baseline of one or more signals sometimes shifted after the crash, which also resulted in unusable files. In general, this problem mainly caused a loss of statistics as the data taking was frequently interrupted.

\paragraph{Logicboxes}
Both logicboxes in use infrequently stopped working, presumably due to memory leaks or overheating. 
The correct reason for this could not be determined.
This also caused a delay in data taking and thus a loss in statistics. 
To cope with this issue, the boxes were placed in a cooling array. 
This helped to reduce the problem. \\

After the experiment was conducted, it was noted that the firmware on the logicboxes during the experiment was not the correct one. 
This fact poses a major uncertainty in our results as we do not know the exact consequences of this. 
It might not have had any influence at all, but it also could have resulted in the LabVIEW crashes or even absolutely unusable data. 

According to Rubio Esteban from the Electronics Workshop at the University of Heidelberg, who is responsible for the firmware of the logic boxes, it was highly unlikely to get the logic boxes to work at all.
%\textcolor{red}{Can't read Nik's feedback...}

%\section{Statistical uncertainties}

%From a statistical point of view it is not explainable why there are close to none signal \textcolor{red}{In figure 8.4 we have approx. 200 events...that's not really close to none or am I on the wrong track??}\textcolor{red}{I think it's probably not statistically significant or in the wrong energy range?} candidates. Regarding the systematic uncertainties, it is likely that the signal candidates are a statistical fluctuation of background events. Possible reasons for the absence of a signal were intensively discussed in the last section. \\
%Nevertheless, we want to briefly discuss the main drivers of statistical uncertainties.
%Statistical uncertainties in our setup in general arise from two major statistical contributions. The first is the branching ratio of the Dalitz decay which is by definition a statistical parameter and thus subject to fluctuations if the amount of data is not big enough. In our case the amount of data taken was about 60000 events and thus large enough. It should have contained up to 600 Dalitz decays, but visible were only 200 signal candidates (\textcolor{red}{reference to figure. Are these the correct numbers?}).
