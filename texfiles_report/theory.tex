\graphicspath{{./figures/theory/}}

\chapter{Theory}

\section{The Standard Model of Particle Physics}

The Standard Model of particle physics (SM) is a theoretical framework for understanding three of the fundamental interactions: the electromagnetic, the weak and the strong interaction. Gravitation is not included in the SM. However, its effect on elementary particles is negligible compared to the other forces.

The SM comprises the fundamental particles of matter, which consists of 12 fermions (spin-$\frac{1}{2}$ particles), the force carriers called gauge bosons (spin-1 particles), and one scalar boson (spin-0 particle), the Higgs boson, \mbox{(see Fig. \ref{fig:smpp}).} The latter is corresponding boson of the Higgs mechanism, which is responsible for the masses of the elementary particles. 

\begin{figure}[H]
	\centering
	\includegraphics[width=.7\textwidth]{smpp.pdf}
	\caption[Standard Model of particle physics]{The fundamental particles of the Standard Model of particle physics \cite{smpp}.}
	\label{fig:smpp}
\end{figure}

The fermions are divided into two groups: quarks and leptons. Each group consists of six particles (as well as their anti-particles), which are arranged in three generations with rising masses.

Each generation of leptons contains a massive negatively charged particle (i) and a corresponding (massless) neutral particle called neutrino ($\nu_i$, i = e, $\mu$, $\tau$). 
The charged leptons participate in the electromagnetic and the weak interaction, the neutrinos only in the latter.

Quarks carry a colour charge (red, green or blue) in addition. 
They are subdivided into up-type quarks, up (u), charm (c), and top (t), that carry an elementary electric charge of $+\frac{2}{3}$, and down-type quarks, down (d), strange (s), and bottom (b), that carry an elementary electric charge of $-\frac{1}{3}$. They participate in the strong, weak and electromagnetic interaction.\\

An interaction between two particles is described via the exchange of a force-carrying boson. Each force is correlated to one or more of these so-called gauge bosons.

The photon ($\gamma$) is the massless mediator of the electromagnetic force and couples to all particles with electric charge. 
The weak interaction is mediated by three massive gauge bosons: the charged $W^\pm$ and the neutral $Z^0$. 
It is unique in the SM because it allows for flavour changing interactions,~i.e.~transitions between the three generations of fermions. 

The 8 gluons ($g$) are the mediators of the strong interaction, which are massless and carry a colour charge. 
They couple to colour charge resulting in self-interactions and interactions with quarks. Due to the short interaction range, quarks and gluons are only observed in a colour confined state.
In nature, these colour neutral states are found to be mesons, consisting of a quark-antiquark pair, or baryons, which are made up of three quarks.


\section{The Neutral Pion $\pi^0$}
The $\pi^0$ is the lightest hadron in the Standard Model of Particle Physics. 
Being a meson, it consists of a quark and an antiquark which is described by the quantum mechanical superposition $\Ket{\pi^0} = \frac{1}{\sqrt{2}}(\Ket{u\bar{u}} - \Ket{d\bar{d}\,})$.
With zero spin and negative parity (\hbox{$J^P = 0^-$}) it is a pseudo-scalar particle and member of the isospin triplet $\pi^0$, $\pi^+$, $\pi^-$, all having an isospin of $\text{I} = 1$. 
It has a mass of $m_{\pi^0} = (134.9766\pm 0.0006)$ \si{\mega\electronvolt} and a lifetime of $\tau =$ ($8.52 \pm 0.18$) $\times\ 10^{-17}$ \si{\second} \cite{pdg}.
\ \\ \\
The decay modes for the $\pi^0$ are all electromagnetic decays (taken from \cite{pdg}):
\begin{itemize}
\item B($\pi^0\rightarrow\gamma\gamma$) = ($98.823\pm 0.034$) \si{\percent} (dominant decay)
\item B($\pi^0\rightarrow e^+ e^-\gamma$) = ($1.174\pm 0.035$) \si{\percent} (Dalitz decay)
\item B($\pi^0\rightarrow e^+ e^- e^+ e^-$) = ($3.34\pm 0.29$) $\times\ 10^{-5}$ 
\item B($\pi^0\rightarrow e^+ e^-$) = ($6.46\pm 0.33$) $\times\ 10^{-8}$ 
\end{itemize}
\ \\
The Feynman graphs of the dominant decay of the neutral pion into two photons via a virtual quark are shown in figure \ref{fig:pi0_gammagamma}. 
The Dalitz decay, which is also called internal conversion, is described in more detail in the following section.


\begin{figure}[H]
	\centering
	\subfigure[t-channel]{
		\includegraphics[width=.42\textwidth]{pi0_gammagamma.pdf}
		%\label{fig:pi0_gammagamma_a}
	}
	\subfigure[u-channel]{
		\includegraphics[width=.4\textwidth]{pi0_gammagamma2.pdf}
		%\label{fig:pi0_gammagamma_b}
	}
	\caption{Leading order Feynman diagrams of $\pi^0\rightarrow\gamma\gamma$.}
	\label{fig:pi0_gammagamma}
\end{figure}

\section{The Dalitz Decay}
The Dalitz decay is the second most probable decay mode of the neutral pion, even though it has a branching ratio of only $(1.174\pm 0.035)$~\si{\percent} \cite{pdg}.
Figure \ref{fig:pi0_eegamma_a} shows the quark and the antiquark of the $\pi^0$ annihilating to a virtual photon which then splits up into an $e^+ e^-$ pair.
One of these leptons is a virtual particle and emits a photon, which results in a final state with a positron, an electron and a photon.
Note that there is another contributing Feynman diagram in which the photon is radiated off the electron.\\
The Feynman diagram resembles the dominant decay $\pi^0 \rightarrow \gamma\gamma$, shown in figure~\ref{fig:pi0_eegamma_b}.
The quark and the antiquark interact via a virtual quark while two photons are created, one of them being real and the other one being virtual.
The virtual photon then converts to an $e^+ e^-$ pair. \\
In the following, the kinematics of the Dalitz decay is described in detail.

\begin{figure}[H]
\centering
\subfigure[s-channel]{
\includegraphics[width=.45\textwidth]{pi0_eegamma.pdf}
\label{fig:pi0_eegamma_a}
}
\subfigure[t-channel]{
\includegraphics[width=.4\textwidth]{pi0_eegamma2.pdf}
\label{fig:pi0_eegamma_b}
}
\caption{Leading order Feynman diagrams of the Dalitz decay $\pi_0\rightarrow e^+ e^-\gamma$.}
\label{fig:pi0_eegamma}
\end{figure}

\subsection{Kinematics}
\label{sec:kinematics}
The Dalitz decay is a three-body decay, which can be approximated to be a two-body decay when assuming that the $e^+ e^-$ pair is emitted collinearly.
The difference between the three-body and the two-body decay is described below.

\subsubsection*{Three-Body Decay:}
The description of the Dalitz decay as a three-body decay is exact.
Momentum conservation yields that, in the rest frame of the $\pi^0$, the decay must lie in a plane.
The momentum balance in this frame is given by:
\begin{equation}
p_{e^-} + p_{e^+} + k_\gamma = (m_{\pi^0}, \vec{0})^T,
\end{equation}
where $p_{e^\pm}$ is the momentum of the electron/positron and $k_\gamma$ is the momentum of the photon.
The quantity $q = p_{e^-} + p_{e^+}$ corresponds to the total four-momentum of the two leptons.

\subsubsection*{Two-Body Decay:}
With the approximation $q^2 = 2m_{e}^2 \approx 0$, the situation simplifies to be a two-body decay in which the two leptons are emitted with a negligible opening angle and can be treated as collinear.
Larger angles are suppressed such that most $\pi^0$ decays will have a small opening angle between the $e^+$ and the $e^-$.

\subsection{Background}
\label{section:background}

External conversion of a photon ($\gamma$) from the dominant decay mode of the neutral pion $\pi^0$ into an electron-positron pair results in the same final state particles as produced by the Dalitz decay: 
\begin{alignat*}{2}
\pi^0\rightarrow \gamma       &\gamma \\
 &\ \rotatebox[origin=c]{180}{$\Lsh$}\gamma\rightarrow e^+ e^- \\
\end{alignat*}
Therefore, these events contribute to the background. 
To distinguish this process from the Dalitz decay, the material thickness, which leads to the external conversion, needs to be varied. See sections \ref{sec:countingrates}, \ref{sec:electronarm} and \ref{sec:externalconversion}.



\section{Pion Production}
\label{section:pion_production}

The $\pi^0$ are produced through so-called charge exchange reactions (\textbf{CEX}).
There are two ways to create $\pi^0$:

\begin{enumerate}
\item $\pi^+ n\rightarrow \pi^0 p$
\item $\pi^- p\rightarrow \pi^0 n$
\end{enumerate}
In this experiment the second method was chosen and an incoming $\pi^-$ was stopped in a $(\text{CH}_2)_n$ target. 
This material was chosen because of its high single proton density. \\
The incoming $\pi^-$ can be caught by a proton in the target material.
Thus, pionic hydrogen ($\pi^- p$) is created in which the CEX reaction occur.
\subsection{Pion Production: Experimental Point of View}
\label{exp_pov}
For the experiment, the reaction $\pi^- p\rightarrow \pi^0 n$ is the preferred choice since the conversion probability can be increased using a target material with a high proton content, which is given by most plastics, including nylon.
The use of plastic as the target and moderator material is beneficial because it is easy and safe to handle and the thickness can be measured precisely.

In addition, a $\pi^-$ beam has a certain contamination of $e^-$.
This is better compared to $e^+$ in terms of background because the positrons may annihilate with electrons inside the moderator or target material, producing pairs of photons which contribute to the photon background.

A downside of this reaction is that the $\pi^-$ flux at the $\pi$M1 beamline is only approximately \SI{10}{\percent} of the $\pi^+$ flux at an energy of $\SI{176}{\mega \electronvolt}$ (see figure \ref{fig:beam_composition}), which increases the required measurement time. 

\subsection{Momentum of the generated Pions}

The pionic hydrogen decays at rest to a $\pi^0$ and a neutron, which leads to a total center-of-mass (CMS) energy of:

\begin{equation}
\sqrt{s} = m_{\pi^-}+m_{p}= E_{\pi^0}+E_{n}=\sqrt{m_{\pi^0}^2 + p_{\pi^0}^2} + \sqrt{m_{n}^2 + p_{n}^2}
\end{equation}

with $\vert\vec{p}_{\pi^0}\vert = \vert\vec{p}_{n}\vert$. 
The momentum of the $\pi^0$ is calculated by:

\begin{equation}
p_{\pi^0} = \sqrt{\frac{\hat{s}^2-4m_{\pi^0}^2m_{p}^2}{4s}} \approx 28.041 \si{\mega\electronvolt},
\label{eq:energy_pion}
\end{equation}

where $\sqrt{\hat{s}} = \sqrt{s - m_{\pi^0}^2 - m_{n}^2}$ is the reduced center-of-mass energy.
So, the $\pi^0$ decay products are boosted.


\subsection{Boosted Pion Decay}
\label{section:boostedpiondecay}

\begin{figure}[H]
	\centering
	\includegraphics[width=.7\textwidth]{bpd_kinematics.pdf}
	\caption{The $\pi^0\rightarrow \gamma\gamma$ decay kinematics in CMS and laboratory frame.}
	\label{fig:bpd_kinematics}
\end{figure}

In the CMS of the $\pi^0$, the photons of the  decay $\pi^0\rightarrow \gamma\gamma$ are emitted back-to-back. In the laboratory frame, the decay products are boosted and emitted under a favoured angle $\alpha$, as shown in figure \ref{fig:bpd_kinematics}. Considering the energy of the $\pi^0$, the velocity and Lorentz factor are given by

\begin{equation}
	\beta = \frac{p_{\pi^0}}{E_{\pi^0}}\approx 0.2034 \ \ \text{and} \ \ \gamma=\frac{E_{\pi^0}}{m_{\pi^0}}\approx 1.0214.
\end{equation} 

The maximum and minimum energy of the decay photons in the LAB frame can be calculated by using the Lorentz transformation of the energies in the CMS:

\begin{equation}
	E_{\gamma, \pm}  = \gamma\frac{m_{\pi^0}}{2}(1\pm a)\approx
	\begin{cases}
	E_{\gamma, \text{max}} = 82.953\ \si{\mega\electronvolt}\\
	E_{\gamma, \text{min}} = 54.912\ \si{\mega\electronvolt}\\
	\end{cases},\ \ \ \ \ a =\beta \cos\theta^*
\end{equation}

The angle $\theta^*$ is defined as shown in figure \ref{fig:bpd_kinematics}. The probability of the energy of the decaying photons is given by an uniform distribution between these values (see fig. \ref{fig:bpd_dist}).
% \textcolor{red}{Matthias: It is GIVEN! See theory / script. }
\begin{figure}[H]
	\centering
	\includegraphics[width=.5\textwidth]{bpd_dist.pdf}
	\caption{Probability distribution of the photon energy of the decay $\pi^0\rightarrow\gamma\gamma$.}
	\label{fig:bpd_dist}
\end{figure}

The opening angle $\alpha$ in the laboratory frame can be calculated by:

\begin{equation}
\cos\alpha = \frac{2E_{\gamma, \text{max}}E_{\gamma, \text{min}} -m_{\pi^0}^2}{2E_{\gamma, \text{max}}E_{\gamma, \text{min}}} = 1-\frac{2}{\gamma^2(1-a^2)}
\end{equation}

The angular distribution of the photons in the laboratory frame is then the Lorentz boosted uniform distribution preferring small angles, which is described by the headlight effect \citep{headlight1, headlight2}. 
%\textcolor{red}{this reference doesn't exist in the bib file...who added this?}
%\textcolor{blue}{Matthias: I didn't, but I just googled it and found a wikipedia and wolfram link which describes it. I think we can remove now this comments :).}
However, there is a natural limit for the smallest possible angle. This minimal opening angle $\alpha_{min}$ for $a\rightarrow 0$ is given by:

\begin{equation}
\alpha_{\text{min}}|_{a\rightarrow 0} = \arccos(1-\frac{2}{\gamma^2})\approx 156.502 \degree\approx 2.732\ \si{\radian}
\label{eq:alpha_min}
\end{equation}

This results in a probability distribution with a peak at $\alpha_{\text{min}}$ and a falling flank towards $\pi$, shown in figure \ref{fig:bpd_alpha}.

\begin{figure}[H]
	\centering
	\includegraphics[width=.5\textwidth]{bpd_alpha.pdf}
	\caption{Distribution of the opening angle $\alpha$ between the photons of the decay $\pi^0\rightarrow\gamma\gamma$.}
	\label{fig:bpd_alpha}
\end{figure}

\section{Counting Rates}
\label{sec:countingrates}
%All rates from lecture are now declared with R instead of E from the lecture and the paper

In the following, the expected $\pi^0$ rates of an experimental apparatus for $\pi^-$ stopping will be explained.
In this experimental setup, the detection rate $R_\text{D}$ of the Dalitz-decay will be given by:
\begin{equation}
R_\text{D} = \text{B}\cdot R_{\text{ACC}},
\end{equation}\\
with the branching ratio of the Dalitz decay B. 
The acceptance of the detection system $R_{\text{ACC}}$ is defined by the detector geometry (see section \ref{section:geometric_acceptance}). 
However, as discussed in section \ref{section:background} the background contribution coming from photon conversion is dominant. 
Therefore, the conversion has to be taken into account as it contributes significantly to the detection rate of signal-like events, which are the detection of both, an electron and a photon.
The rate of the background process $R_\text{C}$ which arises from external conversion is 

\begin{equation}
R_C = R_{\text{ACC}}\cdot (2\cdot C_\text{M} + 2\cdot K_{\text{Cu}} \ t_{\text{Cu}})
\end{equation}

with the conversion probability in copper per thickness $K_{\text{Cu}}$ and the thickness of the copper $t_{Cu}$. 
$C_\text{M}$ contains the conversion probability in all the other materials including the air, moderators, scintillators and the target itself. 
The factor 2 takes into account that both photons are able to convert into an electron-positron pair.
Copper is chosen because the conversion coefficient of photons in copper is relatively high compared to the absorption probability. \\

Consequently, the total event rate can be expressed as the sum of both rates:

\begin{equation}
R = R_\text{D} + R_\text{C} = R_{\text{ACC}} \cdot (\text{B} + 2\cdot C_\text{M} + 2\cdot K_{\text{Cu}}\cdot t_{\text{Cu}}).
\label{eq:totalrate_exp}
{\tiny }
\end{equation}
From this, we get the branching ratio of the Dalitz decay
\begin{equation}
B = \frac{R}{R_{ACC}}- 2\cdot C_M - 2\cdot K_{\text{Cu}}\cdot t_{\text{Cu}}.
\end{equation}

By measuring the final event rate and varying the thickness of the copper plate, we can extract the Dalitz branching ratio $B$ as a fit parameter. 
In order to measure B on a percent level, the other quantities have to be known even more precisely. 
The material thickness can be varied, and the conversion probability $K_{\text{Cu}}$ can be estimated well (see section \ref{sec:externalconversion}).
$C_\text{M}$ has to be calculated very precisely, which requires to understand the material budget very well.
Beforehand, we can use results from other experiments such as Schardt et al. \cite{Schardt:1980qd} in order to determine a value for the expected final event rate $R$ (see chapter \ref{chapter:expectations}).
