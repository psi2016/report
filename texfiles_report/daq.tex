\graphicspath{{./figures/daq/}}

\chapter{Data Aquisition System}
The data acquisition consists of two logicboxes which communicate with a program written in LabVIEW Professional 2013.
In the following paragraphs, the basic idea of the acquisition is explained.

\section{Hardware}
The most important units of our data acquisition system are two logicboxes.
These boxes contain several internal components to process signals (waveforms), which are received via multiple inputs. They have an ADC for the digitization of the analog signal.
The processed signals are then transferred to a computer via USB. \\
Each logicbox has six input channels for data and other inputs and outputs for external trigger signals and additional information such as the `busy' channel, which can be used to stop data taking momentarily.

\subsection{Analog-to-digital Converter}
The Analog-to-digital Converter (ADC) stores the waveform of the incoming analog signal in a digital format. 
In order to store the signal digitally, samples of the waveform are taken in a predefined sampling rate.
This sampling rate of the ADC defines the accuracy of the digitized waveform in time.
The precision of the amplitude is given by the number of bits available for the quantisation.

\subsection{Memory}
A logicbox continuously digitizes the input levels and stores this information in its ring-buffer-like memory. 
Thus, if the memory is overflowing, data is written again to the start of the memory, overwriting old data that was there.
The memory buffer allows to keep data until a trigger is received.
This information takes some time to be processed and is transmitted via an external trigger to the logicboxes. \\
After the logicbox is triggered to save the data, acquiring new data is paused for both boxes while the data is transferred to the computer.

\subsection{Synchronization}
It is important to note that the logicboxes always have to be synchronized as otherwise mismatches in event numbering between the two boxes could occur.
Therefore, each logicbox has a `busy' channel, which indicates that the memory is currently being read out by the computer and which stops acquiring new data for both boxes.
In order to use both boxes simultaneously, we used logic units displayed in figure \ref{fig:logicbox_logic} which fulfill the previously mentioned requirements to preserve the event order. 

\begin{figure}[h]
	\centering
	\includegraphics[width=0.7\textwidth]{LogicboxLogic.pdf}
	\caption{Schematic of the logicbox logic. Each logicbox has six data inputs and one USB data connection for communication with a PC.}
	\label{fig:logicbox_logic}
\end{figure}

\section{Software}
\subsection{Raw Data Format}
Saving the digitized data from the logicboxes is realized using arrays which contain the height of the signal in each entry while the width between the entries is specified beforehand - the smaller the width between the data points, the higher the amount of data points or 'samples' needed to store signals of the same length. Furthermore, in order to save a signal and not to cut it off, a number of `presamples' can be specified, which is a number of data points saved before the actual signal is triggered.

\subsection{Queues}
The data flow from a logicbox is read into a separate part called `queue'.
After reading the data of an event from both boxes, the two queues of the two logicboxes are merged such that all channels of each logicbox are in a common queue.
This process is done in order to prevent the different signals of the boxes from being mixed.
Then, all the waveforms contained in the queue are saved to a binary file on disk.
Additional information, such as a time stamp and run number, is saved in the header of each file. A specific marker between each event is saved as well, in order to be able to separate two consecutive events. 
\\
The queues are used since the processing and saving of each event is time consuming. 
Using this method, new data can be received and piled up while older events are written into the binary file, and thus the readout speed is maximized.
\\
Since each logicbox has its own event ID counter, we also included a check whether the event IDs of both boxes were the same.
If not, the event is discarded since the IDs always have to match as the information is otherwise useless.

\subsection{Issues}
Sometimes, the baseline and the amplitude of certain signals would get shifted by 4 bits, meaning afterwards it was below its actual position.
The data we received from such runs is still usable, however, it is important to mention, since this has to be considered in the analysis.

\section{Data Processing}
\label{section:DataProcessing}
The raw data written in binary format has to be processed further in order to convert the saved pulse shapes into meaningful data. This has been done with a C++ and ROOT\footnote{\href{https://root.cern.ch}{ROOT} is a data analysis framework typically used in particle physics.} script. \\
The script reads in the raw data on an event by event basis and fills waveforms for each channel specified in the data file. The channels can either be a digital signal or an analog calorimeter signal.

\begin{figure}[H]
	\centering
	\subfigure[Before filtering]{
		\includegraphics[width=.45\textwidth]{beforeFFT.png}
		\label{fig:beforeFFT}
	}
	\subfigure[After filtering]{
		\includegraphics[width=.45\textwidth]{afterFFT.png}
		\label{fig:pi0_gammagamma_b}
	}
	\caption{Calorimeter signal before filtering (a) and after the Fourier filter (b). In addition any DC offset was removed. The red lines indicate the integration window.}
	\label{fig:afterFFT}
\end{figure}

To reduce the noise seen in the analog signals they are sent through a Fourier filter that works in three basic steps. First, the Fourier transform of the histogram is calculated using the FFT (fast Fourier transform) method implemented in ROOT. Then, the high frequency noise is effectively cut away by setting all entries above a specified cut-off frequency to zero. In a third step, the histogram is transformed back, but is now missing any high frequency noise. The result of this procedure can be seen in figure \ref{fig:afterFFT}, where the x-axis represents the serially numbered sample (which is corresponding to the time) and the y-axis the amplitude of the signal.\\
In the case of the calorimeter signals, we are interested in the collected charge which in turn corresponds to a certain energy. Therefore, the signal needs to be integrated. Since we do not want to integrate over the noise before and after the pulse, thresholds are set to cut it away. Once the signal exceeds the threshold, the signal is simply summed bin by bin until it again falls below the same threshold. The bin number corresponding to the first passing of the threshold is the arrival time of the signal.

\begin{figure}[h!]
\centering
\includegraphics[width=0.7\textwidth]{multipleSignals.png}
\caption{In some cases multiple digital signals arrived inside one event. A pulse finding algorithm was implemented to save all of them for later analyses.}
\label{fig:multipleSignals}
\end{figure}

In the case of the digital trigger signals only the arrival time is of interest. Often multiple signals can be found in one event, which can be seen in figure \ref{fig:multipleSignals}. Therefore a pulse finding algorithm was implemented which returns the arrival time of the individual signals.\\
All of the calculated values are saved in the end in ROOT trees. 

