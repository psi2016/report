\graphicspath{{./figures/Measurements/}}

\chapter{Measurements}

\section{Scintillator Calibration}


Available scintillators were tested and calibrated with a radioactive source ($^{90}$Sr). 
All scintillators were probed individually and not sandwiched between reference scintillators. 
That is because the emitted radiation energy is approximately \SI{0.5}{\mega\electronvolt}, most of the particles are stopped in the first scintillator already. 

While a source was located on top of the scintillator, the count rate was measured and plotted against the applied voltage as shown in figure \ref{fig:ScintillatorEfficiencies}. 
There are mainly three distinct regions in these curves. 
In the center part, the plateau, the count rate does not vary much when the supply voltage is changed.
This is the desired working region which was around $2.1~\pm$ \SI{0.2}{\kilo\volt} for most scintillators.

\begin{figure}[!h]
	\centering
	\includegraphics[width=.9\textwidth]{S_used_new.pdf}
	\caption{Count rate curves or plateau characteristics of the used scintillators. The vertical bars indicate the operating voltages used.}
	\label{fig:ScintillatorEfficiencies}
\end{figure}



\section{Range Curve}
\label{section:range_curve}
In order to maximize the number of stopped pions in the target, the stopping rate was measured as a function of the moderator thickness and beam energy. 
The stopping rate is measured with the rate of the \texttt{$\pi$-\textit{stop}-trigger} (see section \ref{section:beam_monitor}).
\\
Depending on the incoming beam energy, more or less material is needed to slow down and stop the particles between beam opening and the target, which is described in detail in section \ref{section:components}.
With the CSDA range shown in figure \ref{fig:range_in_lucite} it is straightforward to calculate the necessary moderator thickness. 
The figure shows, for different particles, the inverse of the mass attenuation coefficient in lucite, commonly known as acrylic glass or plexiglass which is very similar to the scintillator material used in the experiment, 
plotted against the kinetic energy of the particle.
One has to calculate the kinetic energy of muons and pions corresponding to the momentum fixed by the beam magnet settings.
The results for \SI{160}{\mega\electronvolt\per c} and \SI{176}{\mega\electronvolt\per c} are summarized in table \ref{tab:range}.

\begin{figure}[H]
	\centering
	\includegraphics[width=.7\textwidth]{CSDA_Rangecurve_All.pdf}
	\caption[CSDA Range in Lucite]{CSDA Range in Lucite, a plastic very similar to the scintillator material. CSDA stands for \textbf{continuous slowing down approximation}. Data taken from \citep{estar} and \citep{grab:beams}. 
	}
	\label{fig:range_in_lucite}
\end{figure}

As the ranges for muons and pions differ significantly for the relevant energies in this experiment, it is highly unlikely that the maximum of the range curve corresponds to muons.
The range of electrons is more than twice as long as that of the pions, thus an accidental optimisation to stop electrons rather than pions can be excluded.
\\
In a first step, the beam momentum was fixed to  \SI{160}{\mega\electronvolt\per c} while the total moderator thickness was varied. 
With this beam momentum a total moderator thickness of \mbox{$11.7~\pm$ \SI{0.1}{\centi\meter}} was optimal to stop the pions in our target.
To this thickness one has to add the scintillator thicknesses (total $3.23~\pm$ \SI{0.1}{\centi\meter}) and consider that the target has also a thickness of $10.9~\pm$ \SI{0.05}{\milli\meter} (see section \ref{section:components_target}). 
Therefore the total stopping length without the target is $14.9 \pm$ \SI{0.1}{\centi\meter}.
The range curve peaks for this value, as can be seen in figure \ref{fig:RangeCurve_1}.
This stopping length was confirmed in a second step by varying the beam energy by $\pm$ \SI{5}{\percent}.
\\
Later the beam momentum was increased to \SI{176}{\mega\electronvolt}, for which a total moderator thickness of $14.45~\pm$ \SI{0.1}{\centi\meter} was chosen.
Adding the scintillator thickness to this one gets a total stopping length (without target) of around $17.7~\pm$ \SI{0.1}{\centi\meter}.
We could confirm this as the optimal thickness by varying the beam momentum, which is shown in figures \ref{fig:RangeCurves_2_and_3}.
The measured values are also in good agreement with the expectation values, as can be seen in table \ref{tab:range}.
\\
It should be mentioned that, using this method, it cannot be ensured that the pions get stopped in the target and not in the scintillator in front of the target already. 
However, because the peak is relatively broad the thickness of \SI{0.705}{\centi\meter} of the third scintillator in the beam may in the worst case cause only a minor shift away from the maximum where the stopping rate is still large.

\begin{table}[H]
\centering
\begin{tabular}{c c c c c}
\toprule
particle & momentum & kinetic energy  & expected range  & measured range\\
 &  [\si{\mega\electronvolt/c}] &  [\si{\mega\electronvolt}] &  [\si{\centi\meter}] & [\si{\centi\meter}]\\
\midrule
$e^-$ & 160 & $\sim$160 		& $39 \pm 1$ &\\			
$\mu^-$ & 160 & $\sim$86 	& $21.1 \pm 0.5$ &\\			
$\pi^-$ & 160 & $\sim$73 		& $14.3  \pm 0.4$ & $14.9  \pm 0.1$\\		
\midrule
$e^-$ & 176 & $\sim$176 		& $42  \pm 1$ &\\			
$\mu^-$ & 176 & $\sim$100 	& $26.2  \pm 0.6$ &\\			
$\pi^-$ & 176 & $\sim$85 		& $18.0  \pm 0.4$ & $17.7  \pm 0.1$ \\			
\bottomrule
\end{tabular}
\caption{Expected stopping ranges for $e^-$, $\mu^-$ and $\pi^-$ in lucite, extrected from figure \ref{fig:range_in_lucite}.}
\label{tab:range}
\end{table}
\newpage
\begin{figure}[H]
	\centering

	\subfigure[$\pi$-Stop Counts]{{\includegraphics[width=.45\textwidth]{RangeCurve_160MeV_Counts.pdf} 	}}
	\qquad
	\subfigure[$\pi$-Stop Fraction]{{\includegraphics[width=.45\textwidth]{RangeCurve_160MeV_Fraction.pdf} 	}}
	\caption{The range curve measurement with fixed momentum of 160 \si{\mega\electronvolt} shows a peak at $14.9~\pm$ \SI{0.1}{\centi\meter}. Material thickness contains scintillator thicknesses ($3.23~\pm$ \SI{0.1}{\centi\meter}) excluding target thickness.}
	\label{fig:RangeCurve_1}
\end{figure}


\begin{figure}[H]
	\centering
	
	\subfigure[$\pi$-Stop Fraction]{{\includegraphics[width=.45\textwidth]{RangeCurve_155mm_1_and_2_Fraction} 	}}
	\qquad
	\subfigure[$\pi$-Stop Counts]{{\includegraphics[width=.45\textwidth]{RangeCurve_155mm_2_Counts.pdf} 	}}

	\caption{Range curve measurement with fixed moderator thickness. Total stopping length is around $17.7~\pm$ \SI{0.1}{\centi\meter} (scintillators and moderators).}
	\label{fig:RangeCurves_2_and_3}
\end{figure}

\newpage
\section{Energy Calibration}
\label{section:energycalibration}
As described in section \ref{subsec:elcalo}, a calorimeter measures the energy of incoming photons or ionizing particles.
However, the energy scale,~i.e. which signal intensity corresponds to which energy, is not known ab initio.
Therefore, an energy calibration is required. \\
\indent The concept was to utilize the Michel decay:
\[ \mu\rightarrow  e {\nu}_e \nu_\mu \]
for the calibration of the $\gamma$-calorimeter (the large calorimeter) because this electron spectrum shows a characteristic and well-defined sharp edge at an energy of $\approx$ \SI{53}{\mega\electronvolt} when radiative corrections are not taken into account.
With radiative corrections this edge at $\approx$ \SI{53}{\mega\electronvolt} gets broader, as can be seen in figure \ref{fig:michel_spectrum}. 
One can use the position of the maximum at $ \approx$ \SI{48}{\mega\electronvolt} and the position of the half maximum on the right side at $\approx$ \SI{51}{\mega\electronvolt} to set two points for calibration.

The trigger logic was set up as follows: "\texttt{$\gamma$-veto} AND \texttt{$\gamma$-sum} AND \texttt{$\pi$-Stop"}.
This ensures that a particle is stopped in the target and the large calorimeter detects a charged particle and not a photon.
This strategy is feasible because not only pions but also a small fraction of muons is stopped in the target.
Nearly all of these muons decay via the Michel process. \\

The measured signal pulses of the calorimeter were integrated in a proper pulse width window, and filled in a histogram where for each value of the pulse integration the number of events is displayed (see chapter \ref{section:DataProcessing} and \ref{section:Michels_Analysis}). 
The $x$-axis can therefore be interpreted as energy in arbitrary units. In order to determine the right energy scale, one has to cut away all events which do not correspond to Michel decays.
After this, a Michel spectrum should be left over like the red solid line in figure \ref{fig:michel_spectrum}.
The analysis of the data from this type of measurement is described in section \ref{section:Michels_Analysis}.

\newpage
\begin{figure}[H]
	\centering
	\includegraphics[width=.8\textwidth]{michel_spectrum_2.png}
\caption[The energy spectrum of the Michel decay.]{{The energy spectrum of the Michel decay. 
The peak of the red solid line is at $\approx$ \SI{48}{MeV}. Citation:
The solid red (dotted blue) line is the spectrum with (without) radiative corrections. 
The green dashed line represents the spectrum of the free muon decay with radiative corrections. 
Near the free muon end point, \SI{52.8}{MeV}, there is a large negative QED correction which pushes the dashed line to zero, and the solid line below the dotted one.
In the low-energy region, magnified in the inset, both radiative (dashed) and binding (dotted) corrections are positive, leading to an increase of low-energy electrons.
The solid line includes both effects. Cited and taken from \citep{Michel:PhysRev}.}}
\label{fig:michel_spectrum}
\end{figure}
