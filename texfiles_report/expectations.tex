\graphicspath{{./figures/expectations/}}

\chapter{Expectations}
\label{chapter:expectations}

Expectations for the event rate are calculated in order to be compared to the data acquired in the experiment. A more detailed discussion of uncertainties can be found in chapter \ref{chapter:uncertainties}.

In section \ref{sec:countingrates} we demonstrated how the final event rate can be determined and which processes can be found in the data. 
The total event rate can be calculated from equation \ref{eq:totalrate_exp}. 
Here we formulate these expectations as accurately as possible.

\section{Acceptance Rate}

The acceptance rate is determined by the rate of incoming particles and by the solid angle of our experiment. 
For a more accurate result we could also include the efficiencies of our detectors, but for an estimate of our event rate this can be neglected as the quantum efficiency of the scintillators is assumed high compared to the geometric acceptance of approximately \SI{1.74}{\percent} of the setup.

Therefore, we can calculate the angular acceptance of our setup and compare it to the angular distribution of the Dalitz decays. The incoming particle rate can be measured with the beam monitors and the pion production efficiency is estimated with a simulation \citep{corrodi:simulation}.


\subsection{Geometric Acceptance}
\label{section:geometric_acceptance}
The geometric acceptance of our detector is defined by the smaller solid angle of the two detector arms, since coincidence in both arms is required. The following calculation is made under the assumption of a point-like target, which serves as an approximation of the real setup.
The solid angle of each arm depends on the area covered by the very last detection unit.
For the photon arm it is defined by the area of a spherical cap of the calorimeter on a unit sphere by $\Omega = A/r^2$ with $A = 2 \pi r^2 (1 - \cos\theta)$ being the surface of a spherical cap.
The distance of the $\gamma$-calorimeter from the target is 57~$\pm$~\SI{1}{\centi \meter} and its aperture has a diameter of 16.4~$\pm$~\SI{0.1}{\centi \meter}.
The opening angle of the spherical cap is $\theta = \tan(r_\gamma/d_\gamma)$, where $d_\gamma$ is the distance to the detector and $r_\gamma$ the radius of the aperture of the calorimeter.
Calculating the solid angle of the $\gamma$-arm, we obtain 
\begin{equation*}
\Omega_{\gamma} = 0.066 \pm \SI{0.003}{sr}.
\end{equation*}

The distance of the very last scintillator in the electron arm from the target is 44~$\pm$~\SI{1}{\centi \meter} with a coverage of $(119 \times 144) \pm (1 \times 1)$ \si{\milli \meter\squared}. The scintillator forms the bottom of a rectangular pyramid with apex angles $\alpha_{ap} = 2 \arctan(h_{sci}/2 d_{e})$ and $\beta_{ap} = 2 \arctan(w_{sci}/2 d_{e})$, where $h_{sci}$ and $w_{sci}$ are the height and the width of the last scintillator respectively and $d_{e}$ the distance from the target to the detector. The solid angle can then be calculated via \cite{solidanglesource}
\begin{align*}
\Omega_{e} &= 4 \arcsin\left(\sin\left(\frac{\alpha_{ap}}{2}\right)\cdot\sin\left(\frac{\beta_{ap}}{2}\right)\right) \\
 &= 0.087 \pm \SI{0.004}{sr}.
\end{align*}
The calorimeter coverage in the photon arm is the limiting factor. \\

As we calculated in section \ref{section:boostedpiondecay}, the decay products of the pion are not distributed homogeneously on a sphere, but on a spherical cap. 
The experiment has an opening angle of $\alpha \approx$~\SI{160}{\degree}, which yields $\Omega_{\pi} = 5.00~\pm$~\SI{0.01}{sr}. \\
Therefore, we expect a fraction of 
\begin{equation}
	R_{ACC} = \frac{\Omega_\gamma}{\Omega_{\pi}} = 1.74 \pm \SI{0.59}{sr}
\end{equation}
of all pion decays to be detected in our experimental setup. As we made the assumption of a point-like target, we expect a geometric acceptance larger than \SI{1.74}{\percent} due to the expansion of the target and the beam.


\section{External Conversion}
\label{sec:externalconversion}

The main background for the searched signal decay arises from external
conversion of a photon from the dominant decay mode of $\pi^{0}$:

\begin{alignat*}{2}
\pi^0\rightarrow \gamma       &\gamma \\
 &\ \rotatebox[origin=c]{180}{$\Lsh$}\gamma\rightarrow e^+ e^- \\
\end{alignat*}
\ \\ To separate this background from the Dalitz-decay, the copper thickness, which leads to the external conversion, has to be varied.
In general, the conversion probability in a given material is

\begin{equation}
C_X=1-e^{-\frac{t}{\lambda}}\label{eq:Probability}
\end{equation}
\ \\
where $t$ denotes the thickness in units of mass per area unit and $\lambda = 1/\mu$ the absorption length. 
$\mu = (\mu/\rho)\cdot\rho$ is the linear attenuation coefficient that depends on the energy of the photons, the atomic number and density $\rho$ of the material and
$\mu/\rho$ is the mass attenuation coefficient. 
The photon energy was calculated before in \ref{section:boostedpiondecay} and lies in the range of $E_{\gamma} \approx 55-$\SI{83}{\mega \electronvolt}.\\
Note that for thin materials (as in our experiment) or large absorption lengths the formula can be approximated by

\begin{equation}
C_X \approx t/\lambda.
\end{equation}
Thus, the conversion rate per thickness $K_X = C_X/t$ becomes a constant value $K_X \approx 1/\lambda$.
This linear relation between $R$ and $t_{\text{CU}}$ is applicable in case of a conversion probability of the order of a few percent and can be used for the background determination.

\subsection{Conversion Probability in Copper}
The mass attenuation coefficient $\mu/\rho$ for copper as a function of photon energy is plotted in figure \ref{fig:copperconv}. For the energies in our experiment we find:
\begin{equation}
\mu/\rho= (4.4-5.2)\cdot 10^{-2}~\si{\centi\meter\squared\per g}
\end{equation} 
Thus, using the density of copper $\rho_{\text{CU}} = 8.96 ~\pm~$\SI{0.02}{g\per\centi\meter^3} \cite{copper} to calculate the absorption length we obtain
\begin{equation}
\lambda = \frac{1}{\mu} = \frac{\rho}{\mu} \cdot \frac{1}{\rho_{\text{CU}}} = (2.14 - 2.54) \; \text{cm}
\end{equation}

\begin{figure}[h]
	\centering
	\includegraphics[width=.7\textwidth]{copperconv.pdf}
	\caption[Mass attenuation coefficient $\mu/\rho$ resp. mass energy-absorption coefficitent $\mu_{en}/\rho$ of copper depending on photon energy]{Mass attenuation coefficient $\mu/\rho$ resp. mass energy-absorption coefficitent $\mu_{en}/\rho$ of copper depending on photon energy \citep{NIST}.}
	\label{fig:copperconv}
\end{figure}
\ \\
For the conversion probability in copper we obtain the values in table \ref{tab:cuconversion}, using the different copper thicknesses we used during the experiment.


\begin{table}[H]
\centering
\begin{tabular}{cc}
\toprule
\textbf{Thickness} $[$\si{\centi\meter}$]$ & \textbf{Conversion probability} $C_{\text{CU}}$ $[$\si{\percent}$]$ \\
\midrule
$0.33$ & $12.2-14.2$ \\
$0.35$ & $12.9-15.0$ \\
$0.66$ & $22.9-26.4$ \\
$0.7$ & $24.1-27.8$ \\
$2.55$ & $63.4-69.5$ \\
\bottomrule
\end{tabular}

\caption{Conversion probability depending on thickness of copper plate for photons with energies $E_{\gamma} \approx 55-$\SI{83}{\mega \electronvolt}.}
\label{tab:cuconversion}

\end{table}

\subsection{Conversion Probability in other Materials}
\label{sec:conversion_other}
Only a small fraction of the trajectory of the emitted photons is in copper, the largest parts they pass through is air, but they also pass through the target and scintillators. 
For each part, there is an additional probability of conversion, which is accumulated in the factor $C_\text{M}$ in equation \ref{eq:totalrate_exp}. 
It is crucial to precisely identify the components which are contributing for an accurate result of the order of better than \SI{1}{\percent}.\\
We have to take into account that multiple materials are passed through, so therefore we multiply the survival probabilities:

\begin{align*}
C_\text{M} &= 1- \exp\left(- \sum_i\frac{t_i}{\lambda_i}\right) \\
& = 1-\exp\left(-\frac{t_{\text{air}}}{\lambda_{\text{air}}}-\frac{t_{\text{plastic}}}{\lambda_{\text{plastic}}}-\frac{t_{\text{scint}}}{\lambda_{\text{scint}}}-\frac{t_{\text{tape}}}{\lambda_{\text{tape}}}-\frac{t_{\text{Al}}}{\lambda_{\text{Al}}}\right)  \\
& \approx \frac{t_{\text{air}}}{\lambda_{\text{air}}}+\frac{t_{\text{plastic}}}{\lambda_{\text{plastic}}}+\frac{t_{\text{scint}}}{\lambda_{\text{scint}}}+\frac{t_{\text{tape}}}{\lambda_{\text{tape}}}+\frac{t_{\text{Al}}}{\lambda_{\text{Al}}} \\
&= C_{\text{\text{air}}}+ C_{\text{plastic}} + C_{\text{scint}} + C_{\text{tape}} + C_{\text{Al}}~.
\label{eq:conversion_other}
\end{align*}
In the following sections each contribution is calculated separately. The trajectory lengths are listed in table \ref{tab:materiallist}.

\begin{table}[H]
\centering
\begin{tabular}{lr}
\toprule
\textbf{Material} & \textbf{Thickness} $[$\si{\centi\meter}$]$ \\
\midrule
Air & $44 \pm 1$ \\ 							
Plastic & $0.6 \pm 0.1$ \\					
Scintillator material $e$-arm &  $1.15 \pm 0.02$ \\		
Tape & $0.2 \pm 0.1$\\					
Aluminium foil & $0.05 \pm 0.1$\\

\bottomrule
\end{tabular}
\caption{Material thicknesses in the trajectory of the photons emitted in the target.}    
\label{tab:materiallist}
\end{table}

\begin{figure}[H]
	\centering
	\includegraphics[width=.7\textwidth]{airconv.pdf}
	\caption[Mass attenuation coefficient $\mu/\rho$ of dry air near sea level depending on photon energy]{Mass attenuation coefficient $\mu/\rho$ of dry air near sea level depending on photon energy \citep{NIST}.}
	\label{fig:airconv}
\end{figure}

\subsubsection{Conversion probability in Air}

The mass attenuation coefficient in figure \ref{fig:airconv} in our photon energy range is approximately constant $\mu / \rho \approx (1.60 \pm 0.05 ) \cdot 10^{-2}$~\si{\centi\meter\squared/g} and thus using the density of dry air $\rho_{\text{air}} \approx$ \SI{0.001204}{g/\centi\meter^3} at room temperature (\SI{20}{\degreeCelsius}) \cite{air}, we find the conversion probability in air (for \SI{44}{\centi\meter} of air):
\begin{equation*}
C_{air} \approx 0.085 \, \pm \SI{0.003}{\percent}.
\end{equation*}

\subsubsection{Conversion probability in Scintillator Material}

Following the same process as above, for the scintillator material we obtain from \mbox{figure \ref{fig:scintconv}}:
\begin{equation*}
\mu/\rho \approx (1.3 \pm 0.2) \cdot 10^{-2}~\si{\centi\meter\squared\per g}
\end{equation*}
 and therefore with a density of \mbox{$\rho_{\text{sci}} \approx 1.0320$ \si{g\per\centi\meter^3}} \cite{sci_density} we obtain $\lambda=74.7 \, \pm$ \SI{0.1}{\centi\meter}.
Consequently, the conversion probability for \SI{1.15}{\centi\meter} of scintillator material is:
\begin{equation*}
C_{\text{sci}} \approx 1.5 \, \pm \SI{0.2}{\percent}
\end{equation*}

\begin{figure}[H]
	\centering
	\includegraphics[width=.7\textwidth]{scintconv.pdf}
	\caption[Mass attenuation coefficient of scintillator material depending on photon energy]{Mass attenuation coefficient of scintillator material depending on photon energy \citep{NIST}.}
	\label{fig:scintconv}
\end{figure}

However, we cannot measure where the photon converted within the scintillator. If the photon converts at the end of the respective detector, it will not be detected and thus this event will not be selected by the trigger logic. This poses an additional uncertainty which is manifested in an overestimation of the background.

\subsubsection{Conversion Probability in Tape}
The scintillators are wrapped in tape to prevent stray light from disturbing the measurement. The total thickness of the tape around both scintillators is estimated to be $2 \pm 1$ mm. The main material within the tape is flexible PVC with a mean density of $\rho_{\text{PVC}} \approx$ \SI{1.2}{g\per\centi\meter^3} \cite{PVC_density}. For the mass attenuation coefficient in figure \ref{fig:pvcconv} we find $\mu/\rho= (3.3 \pm 0.2) \cdot 10^{-2}$~\si{\centi\meter\squared\per g}. Thus, we obtain $\lambda = 25.3 \pm$ \SI{0.1}{\centi\meter}.\\ 
\indent	Finally, the conversion probability in tape for \SI{0.2}{\centi\meter} of material thickness results in:
\begin{equation*}
C_{\text{tape}} \approx 0.8 \, \pm\SI{ 0.4}{\percent}. 
\end{equation*}

\begin{figure}[H]
	\centering
	\includegraphics[width=.72\textwidth]{pvcconv.pdf}
	\caption{Mass attenuation coefficient of PVC depending on photon energy \citep{NIST}.}
	\label{fig:pvcconv}
\end{figure}

\subsubsection{Conversion Probability in Aluminium Foil}
Additionally, under the tape wrapping, the scintillators were wrapped in a thin envelope of aluminium foil. As it could not be measured thickness of this wrapping can only be very conservatively estimated to be $0.5~\pm$ \SI{1}{\milli\meter} with a density of \mbox{$\rho_{\text{Al}} \approx$ \SI{2.7}{g\per\centi\meter^3} \cite{Aluminium}}. Similarly to before we find in figure \ref{fig:aluconv} for the mass attenuation coefficient $\mu/\rho= (2.4 \pm 0.3) \cdot 10^{-2}$ \si{\centi\meter\squared\per g}, so we can compute \mbox{$\lambda = 15.3 \pm$ \SI{0.1}{\centi\meter}}.


\begin{figure}[H]
	\centering
	\includegraphics[width=.72\textwidth]{aluconv.pdf}
	\caption[Mass attenuation coefficient of aluminium depending on photon energy]{Mass attenuation coefficient of aluminium depending on photon energy \citep{NIST}.}
	\label{fig:aluconv}
\end{figure}

For the conversion probability in the aluminium foil for \SI{0.05}{\centi\meter} of material thickness we then obtain: 

\begin{equation*}
C_{\text{Al}} \approx 0.33 \, \pm \SI{0.03}{\percent} . 
\end{equation*}

\subsubsection{Conversion Probability in the Target}
\label{sec:conv_prob_in_target}
Since we do not know where the neutral point decays, we take the mean value of the decay distribution, which is at the center of the nylon target. Therefore, on average photons have to travel through half the target before emerging from the surface.

In figure \ref{fig:plasticconv} we find the mass attenuation coefficient for a similar material and our photon energies to be $\mu/\rho = (1.3 \pm 0.2) \cdot 10^{-2}$ \si{\centi\meter\squared\per g} and with an approximated density of $\rho_{\text{Polyethylene}} \approx$ \SI{0.95}{g\per\centi\meter^3} \cite{Polyethylene} this results in $\lambda = 80.1 \pm$ \SI{0.1}{\centi\meter}. 

\begin{figure}[h]
	\centering
	\includegraphics[width=.7\textwidth]{polyethyconv.pdf}
	\caption[Mass attenuation coefficient of plastic depending on photon energy]{Mass attenuation coefficient of plastic depending on photon energy \citep{NIST}.}
	\label{fig:plasticconv}
\end{figure}
Using \SI{0.6}{\centi\meter} target material thickness, we compute:
\begin{equation*}
C_{target} \approx 0.7 \pm \SI{0.2}{\percent}.
\end{equation*}

\section{Total Event Rate}

The total rate in equation \ref{eq:totalrate_exp} is the sum of the detection rate of the Dalitz-decay and the detection rate of the background from external conversion. Thus, it is crucially dependent on the value of $C_\text{M}$. We calculated each contribution beforehand, so now we can compare the significance of each background contribution in \mbox{table \ref{tab:c_m_comparison}}.

\begin{table}[H]
\centering
\begin{tabular}{cc}
\toprule
\textbf{Material} & \textbf{Conversion probability} $C_\text{M}$ $[$\si{\percent}$]$ \\
\midrule
Air & $0.085 \, \pm 0.003$ \\
Plastic & $0.7 \pm 0.2 $ \\
Scintillator & $1.5 \, \pm 0.2$ \\
Tape & $0.8 \, \pm 0.4$\\
Aluminium foil & $0.33 \, \pm 0.03$\\
\midrule
Total & $3.4 \pm 0.8$\\
\bottomrule

\end{tabular}
\caption{Comparison of background contributions to external conversion.}
\label{tab:c_m_comparison}
\end{table}

\subsubsection*{Expected total event rate depending on the thickness of the copper plate}

Taking the mean value of the conversion probability for each thickness of the copper plate, we calculate the expected total event rate using equation \ref{eq:totalrate_exp} in table \ref{tab:finalrate}. The expected Dalitz branching ratio $\text{B} = 1.25 \pm$ \SI{0.05}{\percent} \citep{Schardt:1980qd} was used.
As an example, one gets the results for 0.33~\si{\centi\meter} of copper using

\begin{align}
\begin{split}
	R & = R_{\text{ACC}} \cdot (\text{B} + 2\cdot C_\text{M} + 2\cdot C_{\text{Cu}}) \\
	  & = \SI{1.74}{\percent}\cdot (\SI{1.25}{\percent} + 2\cdot \SI{3.4}{\percent} + 2\cdot \SI{13.2}{\percent}) \approx \SI{0.6}{\percent}
\end{split}
\end{align}	

Only about \SI{1}{\percent} of the decays that happen are Dalitz-decays, of which only \SI{1}{\percent} lies within the geometrical acceptance of the detector system.
Therefore, the expected rate of measured Dalitz decays is \SI{1}{\percent} $\times$ \SI{1}{\percent} $=$ 1/10,000 of the pion stop rate including the acceptance and the assumption that the two-photon decay of the $\pi^0$ is approximately equally distributed. 



We can conclude, that we need sufficient beam time and/or high $\pi^-$ rates in order to obtain a data set large enough for the analysis. An estimate for the beam time is given below.

\vspace{1cm}
\begin{table}[h]

\centering
\begin{tabular}{cc}
\toprule
\textbf{Thickness} $[$\si{\centi\meter}$]$&  \textbf{Expected Total Decay Rate $R$} $[$\si{\percent}$]$ \tabularnewline
\midrule
0.33 &  $0.6 \pm 0.21$ \\
0.35 &  $0.63 \pm 0.22$ \\
0.66 &  $1.0 \pm 0.35$ \\
0.7 &  $ 1.04 \pm 0.36$ \\
2.55 &  $ 2.45 \pm 0.84$ \\
\bottomrule
\end{tabular}
\caption{Expected total event rate $R$ depending on the copper thickness using the target of \SI{11}{\milli \meter} thickness and the branching ratio B = \SI{1.25}{\percent}.}
\label{tab:finalrate}
\end{table}


\subsubsection*{Required Beam Time}

To obtain an approximation of the beam time needed for an accurate result, we need to make an estimate on the $\pi^0$ production rate. Using simulations that were made beforehand \citep{corrodi:simulation}, we estimate $0.24$ $\pi^0$ per incoming $\pi^-$ as a production rate. \\
Using the $\pi^-$ rate in figure \ref{fig:trigger_logic} we can stop $76,000$ $\pi^-$ per second. Hence, our $\pi^0$ production would be approximately $0.24 \cdot 76,000 = 18,240 $ $\pi^0$ per second, thereof decaying about $230$ via the Dalitz-decay. In an optimistic scenario, using a target thickness of \SI{0.7}{\centi \meter}, we can measure two Dalitz-decays per second. In order to measure the branching ratio of the Dalitz-decay to a percent level, we would need to reduce our statistical error $\sqrt{N}/N$ to \SI{1}{\percent}. Thus, we would need 100,000 observations for this accuracy, which translates into \SI{50000}{\second} or \SI{14}{\hour} of beam time. Taking problems such as efficiencies, beam-down-times, finding the best setting for the experiment etc. into account, the time-span of the experiment is very short.

