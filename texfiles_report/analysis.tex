\graphicspath{{./figures/analysis/}}

\chapter{Analysis}

In this chapter, the analysis strategy is described. Starting with the energy calibration, followed by the weighting of the channels and the background subtraction for a clean sample of Michel decays.
The analysis could not be because of the issues discussed in chapter \ref{chapter:uncertainties}.


The signature of a Dalitz-decay is referred to as the signal.
It corresponds to a coincidence of a pion stop, a detected electron in the electron arm and a signal of a photon in the photon arm.
The whole analysis aims at cutting away all events that does not full fill all of these conditions.
For most of the data acquisition, not the final trigger but another combination of subtriggers has been used because of an extremely low event rate.

\section{Reconstruction cut}
\label{section:RecoCut}
The data is processed by the integrator before analysis, which is described in detail in section \ref{section:DataProcessing}. 
The raw data is converted into root files, which could then be easily analysed with ROOT \cite{RootCern}.
Throughout the analysis it turned out that the analogue sum of the signal of the six single phtotomultipliers (of the large calorimeter) did not exactly match the values given by the integration process. 
To ensure that there is a match, a condition is constructed which is called "Reconstruction Cut" which demands that the ratio of the two values should not differ by more than \SI{20}{\percent}.

\section{Energy Calibration using the Michel Spectrum}
\label{section:Michels_Analysis}
As discussed in section \ref{section:energycalibration}, the calibration of the energy has to be done using a well known energy distribution for which the Michel spectrum is chosen. Identifying the Michel spectrum and its edge in our data allows to calibrate the energy scale of the calorimeter.

As can be seen in figure \ref{fig:Michel_Spectra}, there is no clear edge on the right side. 
Therefore, different cuts in the analysis are added to ensure certain conditions.
One the one hand, these cuts include the conditions which are also set when performing the measurement (for example $\gamma$-veto AND $\pi$-stop) and, on the other hand, the reconstruction cut (see section \ref{section:RecoCut}). 
The spectra after applying the cuts are shown in the same figure \ref{fig:Michel_Spectra}. 

\begin{figure}[H]
\centering
	\subfigure[Michel Spectra A]{{\includegraphics[width=.8\linewidth]{Michel_1.png} 	}}
	\qquad
	\subfigure[Michel Spectra A and B]{{\includegraphics[width=.8\linewidth]{Michel_2.png} 	}}
		
\caption[Michel Spectra with and without cuts and a comparison between the different summing methods A and B.]{Michel Spectra with and without cuts and a comparison between the different summing methods A and B. The single channels sum A is obtained by summing up the raw data of the six channels, whereas the spectra B are summed up earlier in the integration process. There is no clear sharp falling edge on the high energy side, in contradiction with our expectation (see section \ref{section:Michels_Analysis}). The sharp edge on the left hand side originates from the NIM units thresholds.}
\label{fig:Michel_Spectra}

\end{figure}

The remaining part resembles a Gaussian distribution. The Michel spectrum with radiative corrections should not look like a Gaussian distribution as seen in figure~\ref{fig:michel_spectrum}. 
However, the right edge of the Michel spectrum can be approximated by a Gaussian distribution. Therefore, only the right half of the spectrum is fitted with a Gaussian and the maximum and the half maximum on the right side are used as reference points. The single steps are explained in the following. \\

\begin{figure}[H]
	\centering
	\subfigure[Channel 1]{{\includegraphics[width=.8\linewidth]{Michel_CH1.png} 	}}
	\qquad
	\subfigure[Channel 4]{{\includegraphics[width=.8\linewidth]{Michel_CH4.png} 	}}
\caption{Two examples of the single channel Michel spectra with the reconstruction cut and the half Gaussian fit.}
\label{fig:michel_fits}
\end{figure}

In a first step the whole spectrum is used to determine the position of the maximum with a Gaussian function parametrised as:
\begin{equation} 
g(x ; A_i, p_i, \sigma_i) = A_i \cdot e^{- \frac{1}{2} (\frac{x - p_i}{p_2})^2}
\end{equation}
for each channel $i = 1, 2, ..., 6$. These peak positions $p_i$ and other fit parameters are listed in table \ref{tab:Michel_Channel_Weights}. 
In a second step, the position of the maximum is fixed and then only the right side of the maximum is considered again for a second Gaussian fit, as shown in figure \ref{fig:michel_fits}.

This step adjusted the half width at half maximum (HWHM $ = \sigma_i\sqrt{2 \log{2}}$ ) for each channel.
With this one can calculate the position of the half maximum on the right side abbreviated by $H_i$ for each channel $i$ by:
\begin{equation}
H_i = p_i + \sigma_i \sqrt{2 \log{2}} \; .
\end{equation}

The data of the different calorimeter channels are then shifted such that their maximum and half maximum positions would be at the same positions as in channel~1 (the PMT in the center of the calorimeter).
The linear map used for this is:
\begin{equation} 
x'_i(x_j) = p_1 + \sqrt{2 \log{2}}\cdot\sigma_1 \cdot \Big( 1 - \frac{x_j - H_i}{p_i - H_i}\Big) = H_1 + \frac{\sigma_1}{\sigma_i}(x_j - H_i)
\end{equation}
where $x_j$ refers to the old $x$-axis value of the $j$-th channel. One only has to consider the case $i=j$, to not interchange the signals between the calorimeters. 
The linear map fulfills the three conditions:
\[ x'_1(x_1) = x_1 \]
\[ x'_i(H_i) = H_1 \]
\[ x'_i(H_i) - x'_i(p_i) = \sigma_1 \sqrt{2 \log{2}} \].

The contributions from the six channels $i$ is summed up to:
\[ X_{tot} = \sum_{i=1}^6 x'_i(x) \]
for each bin $x \in [0, 25000]$ and then plotted for comparison in figures \ref{fig:Michel_Spectra} and \ref{fig:Michel_Rescaled} (labelled with ``Gamma Sum''). 

\begin{table} [h]
\centering
\begin{tabular}{lccccccr} 
\toprule
					& CH1		& CH2	 & CH3	& CH4	& CH5	& CH6		& Error\\ 
\midrule
Peak $p_i	$			& 10789		&11799	&11768	&7053	&7911	&12523 		& $\pm 100$ \\ 
$\sigma_i	$  			& 3165		&3206	&3052	&1924	&2268	&3312 		& $\pm 20$ \\ 
$H_i	$				& 14520		&15570	&14770	&9320	 &10580	&16420 		& $\pm 120$ \\ 
$\chi^2$ / DoF			& 1.09	& 2.10	& 2.47	& 1.87 & 1.66	& 2.02  & $\pm 0.01$\\ 
$\sigma_i / \sigma_1$	&1 			& 1.01	& 0.96 	& 0.61 	& 0.72  	& 1.05 & $\pm 0.01$ \\ 
\bottomrule

\end{tabular}

\caption[Fit parameters for the half Gaussian fits of the six channels in the Michel spectra.]{Fit parameters for the half Gaussian fits of the six channels in the Michel spectra. $H_i$ refers to the point at half height on the right side of the peak before shifting in channel $i$.
After the shift and rescale all the $H_i$ positions are approximately at $H_1\approx14500$, and the peaks are at $p_1 \approx 10800$.}

\label{tab:Michel_Channel_Weights}
\end{table}

\begin{figure}[H]
	\centering
	\subfigure[without cuts]{{\includegraphics[width=.8\linewidth]{Michel_3.png} 	}}
	\qquad
	\subfigure[with cuts]{{\includegraphics[width=.8\linewidth]{Michel_4.png} 	}}
\caption{Michel spectra before and after rescaling.}
\label{fig:Michel_Rescaled}
\end{figure}

\section{Software Calibration of the Calorimeter}

The different channels of the calorimeter have to be weighted, because due to geometrical considerations the photon distribution varies slightly for each calorimeter. Two different methods are used to evaluate the weights of the channels.

\subsection{Linear Regression}

The first method used linear regression on scatter plots which plotted channel $i$ against channel $j$. Using this method, the relative weight between $i$ and $j$ for all combinations of $i$ and $j$ can obtain. If one denotes $w_{ij}$ the weight between channel $i$ and $j$, then \[w_{ik}=w_{ij}*w_{jk} \; .\]Averaging over all intermediate channels will then produce a mean weight between $k$ and $i$. This can then be normalized in such a way that the sum of weights of all channels $k$ relative to channel $i$ is equal to 6. This result can then again be averaged over all $i$ to obtain the final weight of channel $k$. \newline
An advantage of this method is that it can be applied to any file, regardless of what trigger is used. This allows to even check whether the weighting changes over time.
\begin{figure}[H]
\centering
\includegraphics[scale=0.5]{Energy_Spectrum_after_linreg.png}
\caption{Energy spectrum before and after weighting channels using linear regression. In blue before the calibration, in red after.}
\label{fig:linear_regression}
\end{figure}


Figure \ref{fig:linear_regression} shows that after the calibration, the peak in the energy spectrum is sharper as well as reduced. The falling spectrum up to 110'000 (a.u.) is approximately unchanged, whereas the edge is significantly sharper. This improves the background reduction and thus should result in a clearer signal.

\subsection{Minimization of Peak Width}

Cuts are applied to the runs where only cosmic particles are measured. 
One of them is the reconstruction cut. Although this then only contained one third of the original data, this cut resulted in a clear peak around 130'000 (a.u.) (see figure \textcolor{red}{?} ). 
The width of the peak is minimised with this data, yielding the results in table \ref{tab:minimizationpeakwidth}: 
\begin{table} [htpb]
\centering
\begin{tabular}{cccccc}
\toprule
w1&w2&w3&w4&w5&w6\\
\midrule
1.13576 & 1.034 & 1.12747 & 1.03648 & 0.632663  & 1.03362\\
\bottomrule
\end{tabular}
\caption{Results of the calibration for the different weights.}
\label{tab:minimizationpeakwidth}
\end{table}

However, results obtained by matching the peaks of the distributions in the single channels are different (see table \ref{tab:minimizationpeakwidth2}), which indicates a problem with the data.


\begin{table} [htpb]
\centering
\begin{tabular}{cccccc}
\toprule
w1&w2&w3&w4&w5&w6\\
\midrule
0.899384  & 0.857143 & 0.929936 & 1.46488 & 1.26957  & 0.850485\\
\bottomrule
\end{tabular}
\caption{Results of the calibration for the different weights, obtained with matching the peaks.}
\label{tab:minimizationpeakwidth2}
\end{table}




\section{Background Subtraction}

To see the signal more clearly, it is necessary to determine the beam background. 
Some measurements are performed without target and some with target for the cosmic background. This spectrum can be seen in figure \ref{fig:results} in red in the bottom right.
Before subtracting the background data from the signal, the without-target-signal is normalised to the cosmic-particle-signal at high energies. To normalise the signals, an integration over the high energy regime is performed, requiring that the integrals are the same.
The resulting background can then be subtracted from the signal.\newline
Additionally, the measured the Michel spectrum in the photon arm (see figure \ref{fig:results}, bottom left) is used and added it to the background without target, in order to account for the muons falsely identified as pion candidates. Adding both spectra, it is expected that the data shows an excess within the energy region of approximately 120-140 (a.u.) since a surplus is observed in data in this region.\\
However, in figure \ref{fig:results} at the top it is shown that the data does not fulfill this expectation. An excess in the events can be seen between both 20-40 (a.u.) and 100-120 (a.u.), but not within the expected region. The surplus in the lower energy region can be disregarded as it is close to the falling edge of the spectrum and thus cannot be produced by Dalitz-decays. The peak between 100-120 (a.u.), however, cannot be explained and hence poses the main problem of this experiment.



\begin{figure}[!h]
	\centering
	\includegraphics[width=1.05\textwidth]{results.pdf}
	\caption[The full spectrum (top) compared to the background contributions (bottom).]{The full spectrum (top) compared to the background contributions (bottom). The spectrum without target and the michel spectrum are normalized in the top figures to the full spectrum. The normalization is done with an integration within a certain bin range: the spectra are rescaled such that their integral is the same as the integral from the signal within the same bin range. For the background contribution the high energy regime is chosen (bins 150'000 - 230'000), whereas for the michel spectrum a lower energy regime is chosen (bins 62'000 - 78'000). The rescale factors are then $4.1 \pm 0.2$ for the background and $34 \pm 1$ for the Michel spectrum.}
	\label{fig:results}
\end{figure}

Before subtracting the background, the requirement is that the data fulfills the reconstruction cut condition and that there are two coincident signals in the two scintillators of the electron arm, so both of the signals has to be within a small time interval. This coincidence is crucial for the performed measurement.
This coincidence cut, although chosen to be not too narrow, left in the end almost no events for the background and a very small amount of events in the signal region as it can be seen in figure \ref{fig:spectrum_with_cuts} b) and c). The main peak in these figures is not explainable, as already stated, and within the signal region approximately two events can be found which is a statistically negligible amount of data.
As the information about the Dalitz-decay cannot be extracted from these spectra, the important sources of uncertainties and errors has to be discussed.

\begin{figure}[h]
	\centering
	\subfigure[Background]{{\includegraphics[scale=0.45]{sum_gamma_pistop_minus_notarget_A.pdf} 	}}
	\qquad
	\subfigure[Signal and background]{{\includegraphics[scale=0.45]{sum_gamma_pistop_minus_notarget_B.pdf} 	}}
	\qquad
	\subfigure[Signal]{{\includegraphics[scale=0.45]{sum_gamma_pistop_minus_notarget_C.pdf} 	}}
	\caption{a) The background contribution without cuts.
		b) Comparison between the signal with cuts and background contributions with cuts.
		c) The signal with cuts. }
	\label{fig:spectrum_with_cuts}
\end{figure}