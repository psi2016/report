\graphicspath{{./figures/mechanical_setup/}}

\chapter{The Experimental Setup}

The basic idea behind the experimental setup is to moderate an incoming beam of negative pions ($\pi^-$) such that the they stop in a target.
In the target, neutral pions are formed via a CEX reaction as described in section \ref{section:pion_production}.
The daughter particles of the Dalitz-decay, an electron, a positron, and a photon are then detected in two dedicated detector arms.

The photon is detected in the photon arm, consisting of a large electromagnetic calorimeter and a scintillator as a veto.
On the other hand, the electrons and positrons, which are -- to a good approximation -- emitted collinearly, are detected in the electron arm, which comprises a varying number of copper plates, two scintillators and a small calorimeter.
The purpose of all components and subdetectors is described in detail below. \\

A schematic of the detector arrangement is shown in \hbox{figure \ref{fig:mech_setup}}.
The angle $\alpha \approx 160 \degree $ between the electron arm and the photon arm has been chosen according to theoretical calculations in section \ref{section:boostedpiondecay} (see equation \ref{eq:alpha_min}) and
due to spatial constraints, which do not allow for greater angle. 

The experiment is located in the \textit{$\pi$M1} area in the experimental hall at PSI, as can be seen in figure~\ref{fig:PSIHall}. 
In the following, the individual components are described in detail.

\begin{figure}[h]
	\centering
	\includegraphics[width=.8\textwidth]{mechanical_setup.pdf}
	\caption{\small{The experimental setup.}}
	\label{fig:mech_setup}
\end{figure}

\begin{figure}[!h]
	\centering
	\includegraphics[width=.6\textwidth]{PSIHall.pdf}
	\caption[Position of the experiment in the experimental hall at PSI]{Position of the experiment in the experimental hall at PSI \citep{exp_hall}.}
	\label{fig:PSIHall}
\end{figure}

\section{Components}
\label{section:components}
\subsection{Beam}
PSI provides a proton beam of \SI{590}{\mega\electronvolt} energy at a current of up to \SI{2.3}{\milli\ampere} (corresponding to a beam power of \SI{1.3}{\mega\watt}) which passes through a spallation target for pion production \citep{psi:protonaccelerator}.
During the experiment, the beam current is 2~$\pm$~\SI{0.1}{\milli\ampere}. \\
For the experiment, pions from target M (\SI{5}{\milli\meter} graphite) are used, which are available in the \textit{$\pi$M1} beamline.
This beamline provides a pion beam with momenta between 100 and 500 \si{\mega\electronvolt\per c} \citep{piM1}.
The secondary beam does not only contain pions but also muons and electrons. The specific beam composition depends on the momentum, as can be seen in figure \ref{fig:beam_composition}.
It is bunched with a spacing of \SI{19.75}{\nano\second} \citep{grab:beams}.
By reversing the polarity of the dipole magnet currents, one can switch between $\pi^{+}$ and $\pi^{-}$.

\begin{figure}[H]
	\centering
	\includegraphics[width=.7\textwidth]{beam_composition.pdf}
	\caption[Pion and electron fluxes in $\pi$M1]{Pion and electron fluxes in $\pi$M1 \citep{pi_el_flux}.}
	\label{fig:beam_composition}
\end{figure}

\begin{figure}[h]
	\centering
	\includegraphics[width=.6\textwidth]{Moderators.jpg}
	\caption{The setup of the moderators and scintillators in front of the target.}
	\label{fig:Moderators}
\end{figure}



\subsection{Moderators}
After the particles leave the beam pipe they first pass through two scintillation detectors (\texttt{b\_sci1} and \texttt{b\_sci2}) used as a beam monitor, see section \ref{section:beammonitor}, and then propagate through a sequence of polyethylene plates. 
The plates (see figure \ref{fig:Moderators}) are used as moderators,~i.e.~they slow the particles down in a controlled manner to optimize the pion stopping rate in the target.
It is not reasonable to choose the lowest available momentum of \SI{100}{\mega\electronvolt} to be able to use a thin moderator because the beam composition changes drastically with momentum as can be seen in figure~\ref{fig:Moderators}. In addition the particle rate drops even more drastically.
The thickness of the plates varies between \SI{2.5}{\milli\meter} and \SI{9.95}{\milli\meter}.
The three scintillation detectors in front of the target also contribute to the moderation. 
Their total thickness is 3.23~$\pm$~\SI{0.02}{\centi\meter}.


\subsection{Target}
\label{section:components_target}
After the negative pions pass through the first three scintillation detectors (\texttt{b\_sci1}, \texttt{b\_sci2} and \texttt{b\_sci3}) and the moderators, they hit the target (a nylon square plate). 
The moderator thickness is chosen such that most of the pions stop in the target.
Nylon was chosen as the stopping target material since it has a high proton density for the reaction $\pi^- p\rightarrow \pi^0 n$. 
The thickness of 10.9~$\pm$~\SI{0.05}{\milli\meter} is thick enough to have a range in which the $\pi^-$ can stop and react (see also section \ref{section:range_curve}).
To increase the stopping probability, the target thickness was once increased during the experiment to two equally sized square plates.
The two plates were stuck together with PVC tapes, such that their areas exactly overlapped, yielding a  total target thickness of 21.8~$\pm$~\SI{0.1}{\milli\metre}.
Most of the decay products (mainly photons) can still leave the target, because the conversion probability for this thickness is relative low (see also section \ref{sec:conv_prob_in_target}).
Once the decay products of the pion leave the target, they are detected by the detectors (\texttt{e\_sci1}, \texttt{e\_sci2}) on the electron arm or photon arm side (\texttt{y\_veto}). \\
The target is not perpendicular to the beam line, but rotated by an angle $\beta \approx 128 \degree $, which can be seen in figure \ref{fig:mech_setup}. 
In this way, the particles created in the target with a fixed angle from the boosted decay of the neutral pion will propagate through less and a more uniform amount of material before emerging from the surface compared to a setup where the target is placed perpendicular to the beam. 
In addition, the effective target thickness, which is relevant for stopping the pions, is increased.


\subsection{Scintillators}
In general, one distinguishes between organic and inorganic scintillators.
The principles of operation of these two types are fundamentally different.
While the scintillation of organic scintillators is due to the delocalized $\pi$-electrons, the function of inorganic scintillators is based on energy transitions across the band gap of the crystal they are made from and the recombination of electron-hole pairs.
Scintillating materials emit photons when charged particles traverse it. 
To make sure that scintillation photons do not get re-absorbed just after the emission one typically mixes a wavelength shifter into the material of inorganic scintillators. 
Organic scintillators also work without a wavelength shifter due to the Stokes shift.
This absorbs the photons and re-emits them at a slightly lower frequency for which the material is mostly transparent \citep{kolanoski:2016}. \\ \\
In this experiment, organic plastic scintillators of different shapes and sizes are used. They are connected to photomultiplier tubes via light guides for readout, which is shown below. The electromagnetic calorimeter consists of an inorganic crystal and is described below.

\subsection{Electromagnetic Calorimeter}
\label{subsec:elcalo}
A calorimeter is a device to measure the energy of particles by absorbing them.
Electromagnetic calorimeters are sensitive to electromagnetically interacting particles, i.e.~photons and all charged particles.
The particles interact inelastically while passing through the detector material and thus lose all their energy until they are eventually stopped.


Due to bremsstrahlung and pair production a particle shower evolves.
This leads to scintillation light or ionization of the detector medium (if gaseous or liquid) and can be detected.
The signal is approximately proportional to the energy deposition.

In general, there are two different types of calorimeters:\\
In \textbf{homogeneous calorimeters}, the active detector volume, which is responsible for the signal generation, is equivalent to the passive volume which is where the electromagnetic shower evolves.\\
The other type is called \textbf{sampling calorimeter}, for which one uses alternating slices of an absorber material (passive, shower development) and an active material in which the signal is generated by ionization or scintillation \citep{kolanoski:2016}.


The $"\gamma$-calorimeter$"$ used in this experiment is a homogeneous calorimeter consisting of two closely attached large NaI crystals. 
In the NaI, passing photons or charged particles produce electromagnetic showers that lead to scintillation light.
This scintillation light is detected by 6 (originally 7, but one is broken) photomultiplier tubes that are mounted on the back side of the crystal. 
To minimize the number of particles entering from the sides, the whole detector is shielded in lead except for a small entrance window in the front.\\
The small calorimeter in the electron arm is working in exactly the same manner.
The only differences are that the NaI crystal is much smaller and there is no lead shielding around it.
Because it is too small to contain typical electromagnetic showers in the energy range of the experiment, only a part of the energy of an incoming particle is detected. The rest of the energy is lost (leakage).


\subsection{Photomultiplier Tubes}
Photomultipliers (PMTs) are very sensitive devices for light detection.
When an incident photon hits the photocathode it releases electrons (photoelectric effect), that are multiplied by a cascade of dynodes towards the anode when a high voltage is applied between the cathode and anode.
At the anode they give a negative current pulse which can be read out \citep{kolanoski:2016}.\\
In the experiment, PMTs as shown in figure~\ref{fig:PMTandSci} are used to detect the scintillation photons.

\begin{figure}[h]
	\centering
	\includegraphics[width=.9\textwidth]{PMTandSci.pdf}
	\caption[Schematic of a PMT coupled to a scintillator used for photon detection]{Schematic of a PMT coupled to a scintillator used for photon \mbox{detection \citep{wiki:PMTandSci}.}}
	\label{fig:PMTandSci}
\end{figure}


\section{The Beam Monitor}
\label{section:beammonitor}
The beam monitor consists of two scintillators (\texttt{b\_sci1} and \texttt{b\_sci2}) just behind the exit window of the beam pipe. 
In coincidence, they indicate the rate of incoming charged particles. 
A third scintillator is placed behind the moderator to measure how many particles make it through the moderator without being stopped or scattered sideways.
A forth scintillator is located behind the target.
It acts as veto to ensure that charged particles are stopped in the target.


\section{The Electron Arm}
\label{sec:electronarm}
The electron arm is made up of a slice of copper (one or two plates, each with thickness 3.3~$\pm$~\SI{0.02}{\milli\metre}) followed by two plastic scintillators (\texttt{e\_sci1} and \texttt{e\_sci2}) in coincidence and a small electromagnetic calorimeter in the end.
In the copper, a fraction of the incoming photons is converted into electron-positron pairs such that photons can be measured indirectly. 
Electrons pass through the copper plate mostly without being absorbed. 
Copper has a high efficiency for converting photons to electrons (in our experiment $\approx$~\SI{13}{\percent} for one copper plate at a photon energy of $\approx$~\SI{70}{\mega\electronvolt}).
Varying the thickness of the copper changes the conversion rate and thus the number of converted electron positron pairs. \\
It is important to understand the material budget in the electron arm very well and account for each contribution, such that the conversion probability of all other materials ($C_M$) can be calculated as precise as possible.
Each material contributes to an additional conversion probability of the photons. 
The calculation of this is presented in detail in sections \ref{sec:externalconversion} and \ref{section:range_curve}.

\pagebreak
\section{The Photon Arm}
The photon arm consists of a scintillator and a set of 6 electromagnetic calorimeters.
Incoming photons and charged particles produce electromagnetic showers in the calorimeters. 
The scintillator, which does not detect passing photons, acts as a veto to make sure that the electromagnetic shower in the calorimeter is not caused by a charged particle. 
\newline
The final setup is depicted in figures \ref{fig:SetupFront} and \ref{fig:SetupFront_wElArm} below.


\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{SetupFront.jpg}
	\caption{Front view of the setup with the photon arm (blue) and electron arm (yellow), which form an angle of $\alpha\approx$~\SI{160}{\degree}. The target is marked in red.}

	\label{fig:SetupFront}
\end{figure}

\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{IMG_3593.jpg}
	\caption[Upstream view of the setup.]{Upstream view of the setup. Beam comes from the silver aperture surrounded by a red quadrupole out of the picture (red arrow). 
	It passes through the first two scintillators, the moderators and then through the third scintillator before it hits the target (encircled in blue).}
	\label{fig:SetupFront_wElArm}
\end{figure}

