# Author: Matthias Oscity
import numpy as np
import pylab as pyl
from scipy.optimize import curve_fit

# Read Data


files = ['./StopRange_electrons_in_Lucite.csv','./StopRange_muons_in_Lucite.csv','./StopRange_pions_in_Lucite.csv']    
# Datas should be in same directory as python script
savepath = '../texfiles_report/figures/Measurements/'             

print '------------------------------'
print '     Stop Ranges in Lucite    '
print '------------------------------'
print 'Data from Estar + Grab Slides '
print 'With help of PlotDigitzer     '
print '------------------------------'
print '------------------------------'
# Fit Data
def fitfunc(x,a,b,c):
    return a*x**2+b*x+c

for j in range(len(files)):
    
    fn0 = files[j]    
    print '-------------'
    print 'File Name:', fn0
    #print 'Reading files...'
    
    if j ==0:
        data = pyl.loadtxt(fn0,delimiter=',')
        energy = data[:,0]              # Energy in MeV 
        stop_range = data[:,1]          # Stopping Power in MeV*cm^2/g
        csda_range = data[:,2]          # CSDA Range in g/cm^2
    
    if j > 0:
        data = pyl.loadtxt(fn0,delimiter=',',skiprows=5)
        energy = data[:,0]              # Energy in MeV
        csda_range = data[:,1]          # Range in g/cm^2


    if j == 0:
        da0 = energy
        rda0 = csda_range
    elif j ==1:
        da1 = energy
        rda1 = csda_range
    elif j ==2:
        da2 = energy
        rda2 = csda_range
        
    rho = 1.18          # Density of Lucite in g/cm^3
    
    
    # Done Reading Data
    #print 'Reading files...Done.'
        
    if j == 0:      # Fit Range for electrons in lucite
        r1 = 65     # Lower Boundary for fit
        r2 = 70     # upper Boundary for fit
        Add = 'Electrons_in_lucite'
        titlename=  'CSDA Range of electrons in Polymethyl Methacralate (Lucite, Perspex, Plexiglass)'
    
    if j == 1:      # Fit Range for muons in lucite
        r1 = 32     # Lower Boundary for fit
        r2 = 38     # upper Boundary for fit
        Add = 'muons_in_lucite'
        titlename=  'CSDA Range of muons in Polymethyl Methacralate (Lucite, Perspex, Plexiglass)'
        
    if j == 2:      # Fit Range for muons in lucite
        r1 = 30     # Lower Boundary for fit
        r2 = 48    # upper Boundary for fit
        Add = 'pions_in_lucite'
        titlename=  'CSDA Range of pions in Polymethyl Methacralate (Lucite, Perspex, Plexiglass)'
    
    popts, pcovs = curve_fit(fitfunc,energy[r1:r2],csda_range[r1:r2])
    pcovs = np.sqrt(np.diag(pcovs))
    print 'Fit with a x^2 + b x + c in energy range ', energy[r1], 'MeV - ', energy[r2], 'MeV'
    print 'Fit Parameters a,b,c Error [%]: ', round(abs(pcovs[0]/popts[0])*100,4), ' , ', round(abs(pcovs[1]/popts[1])*100,4), ' , ', round(abs(pcovs[2]/popts[2])*100,4)
    print 'Fit Ratios a/b, a/c : ', abs(round(popts[0]/popts[1],4)), ', ', abs(round(popts[0]/popts[2],4))
    print 'Fit Ratios b/a, b/c : ', abs(round(popts[1]/popts[0],4)), ', ', abs(round(popts[1]/popts[2],4))

    
    # Create Plot
    #print '-------------'
    #print 'Creating Plot...'
    
    
    pic = plt.figure(figsize=(10,8)) # Create Figure of size Width x Height
    plt.rc('legend',**{'fontsize':18}) # Change fontsize of legend
    
    plt.plot(energy,csda_range,color='k',linewidth=1.0, label='Data') # Plot Data
    
    #plt.bar(energy[r1],5,energy[r2-1]-energy[r1],bottom=20,label='Fit Range') # Plot Fitrange
    #plt.plot(energy[r1:r2],fitfunc(energy[r1:r2],*popts),color='r',linewidth=1.0, label='Fit') # Plot Fit
    
    
    plt.xlim([1,250])                                 # Set boundaries for x-axis
    
    plt.title(titlename,y=1.05,fontsize=22)
    plt.xlabel('Kinetic Energy [MeV]',fontsize = 20)            # Label axes
    plt.ylabel('CSDA Range [g/cm^2]',fontsize = 20)
    plt.xticks(fontsize=16)                             # Change font size of x and y ticks
    plt.yticks(fontsize=16) 
    plt.grid(True)
    
    #plt.legend(bbox_to_anchor=(0.99, 0.99), loc=1, borderaxespad=0.)    # Create Legend
    plt.savefig(savepath + 'CSDA_Rangecurve_' + Add + '.pdf' ,bbox_inches='tight') # Save File
    plt.close()     # Close File
    del(pic)        # Clear Memory
    
    #print 'Creating Plot...Done.'
    print '-------------------------------------------------------------------------'
    
    
    # PICK HERE THE ENERGIES YOU LIKE
    ElectronEnergies = [160,176]
    MuonEnergies = [86,100]
    PionEnergies = [73,85]
    
    print 'Energy [MeV]\t ||  CSDA Range [g/cm^2]\t || Expected Range [cm]'
    print '-------------------------------------------------------------------------'
    if j ==0:
        Energies = ElectronEnergies
    if j ==1:
        Energies = MuonEnergies
    if j ==2:
        Energies = PionEnergies
        
    for en in Energies:
        print en, '\t\t ||  ', round(fitfunc(en,*popts),2), '+/- ', round(fitfunc(en,*popts)*abs(pcovs[1]/popts[1]),3), '\t\t ||  ', round(fitfunc(en,*popts)/rho,3), '+/- ', round(fitfunc(en,*popts)*abs(pcovs[1]/popts[1])/rho,3)
        


pic = plt.figure(figsize=(10,8)) # Create Figure of size Width x Height
plt.rc('legend',**{'fontsize':18}) # Change fontsize of legend

plt.plot(da0,rda0,color='k',linewidth=1.0, label='Electrons')
plt.plot(da1,rda1,color='b',linewidth=1.0, label='Muons') 
plt.plot(da2,rda2,color='r',linewidth=1.0, label='Pions') 

plt.xlim([1,230])                                 # Set boundaries for x-axis

plt.title('Range in Lucite \nI= 73.8eV, Z = 6, A = 100, d = 1.18 g/cm^3',y=1.05,fontsize=22)
plt.xlabel('Kinetic Energy [MeV]',fontsize = 20)            # Label axes
plt.ylabel('Range [g/cm^2]',fontsize = 20)

locs, labels = plt.xticks()
plt.xticks(np.arange(locs[0],locs[-1],(locs[1]-locs[0])/2.0)) # Half Grid Spacing in x
plt.xticks(fontsize=16)                             # Change font size of x and y ticks
plt.yticks(fontsize=16) 
plt.grid(True)

plt.legend(bbox_to_anchor=(0.01, 0.99), loc=2, borderaxespad=0.)    # Create Legend
plt.savefig(savepath + 'CSDA_Rangecurve_All.pdf' ,bbox_inches='tight') # Save File
plt.close()     # Close File
del(pic)        # Clear Memory
    
print '-------------'    
print 'Done'