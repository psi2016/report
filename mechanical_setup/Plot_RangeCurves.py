import numpy as np
import matplotlib.pyplot as plt
import pylab as pyl

# Author: Matthias Oscity
PlotWidth = 10                              # Figure Width
PlotHeight = 7                              # Figure Height
SavePath = '../texfiles_report/figures/Measurements/'
    
def plotitsimple(x,y,xerr,yerr,xlabel,ylabel,title,savename,**kwags):

    pic = plt.figure(figsize=(PlotWidth,PlotHeight))    # Create a figure and call it pic

    if ('plotlabel' in kwags):
        plotlabel = kwags['plotlabel']
        plt.errorbar(x,y,xerr=xerr,yerr=yerr,fmt='s',color='k',label=plotlabel)              # Plot Data
        plt.legend(bbox_to_anchor=(1, 1), loc=1, borderaxespad=0.)
    else:
        plt.errorbar(x,y,xerr=xerr,yerr=yerr,fmt='s',color='k')              # Plot Data
    plt.title(title,y=1.05,fontsize=20)
    plt.xlabel(xlabel,fontsize=18)
    plt.ylabel(ylabel,fontsize=18)
    plt.xticks(fontsize=16)
    plt.yticks(fontsize=16)
    if ('xticks' in kwags):                     # This optional argument is for x-ticks  
        arg = kwags['xticks']                   # xticks=2.0 will double x ticks
        locs, labels = plt.xticks()
        plt.xticks(np.arange(locs[0],locs[-1],(locs[1]-locs[0])/arg))
    plt.grid(True)                              # Turn on Grid
    plt.savefig(savename,bbox_inches='tight')   # Save Figure
    plt.close()                                 # Close and Delete
    del(pic)

 
print '----------------------'
print 'Range Curve Measurements'

for i in range(1,4):
    run = str(i)
    
    fn0 = 'Range_curve_' + run + '.txt'
    print '--------'
    # Open Data
    try:
        datas = pyl.loadtxt(fn0,skiprows=1)[:,:]
        print 'Run', run, '- OK.'
    except IOError:
        print 'Run', run, 'could not be found.'

        continue
    
    if i==1:
        x = datas[:,0]
        y0_1 = datas[:,3]
        y0_2 = datas[:,5]
        print 'Maximum Value =', round(max(y0_2)*100,1), '% at ', x[np.argmax(y0_2)], ' cm'
        plotitsimple(x,y0_1/1000.0,0.1,0.01*y0_1/1000.0,'Material Thickness [cm]','Pi Stop Rate [kHz]','Range Curve',SavePath+'RangeCurve_160MeV_Counts.pdf',xticks=2.0)
        plotitsimple(x,y0_2*100.0,0.1,0.01*y0_2*100.0,'Material Thickness [cm]','Pi Stop Fraction [%]','Range Curve',SavePath+'RangeCurve_160MeV_Fraction.pdf',xticks=2.0) 
        # For Normalized Plot (Normalized by taking Maximum and set it to 99%)
        # Uncomment the line below
        #plotitsimple(x,y2/y2.max()*0.99*100.0,0.1,0.01*y2/y2.max()*0.99*100.0,'Material Thickness [cm]','Pi Stop Fraction [%]','Normalized Range Curve (160 MeV)','RangeCurve_160MeV_Ratio_Normalized.pdf',xticks=2.0) 

    if i==2: # This run is from 4.9.2016, Nightshift, ELOG Entry #41 : http://pomeron.physi.uni-heidelberg.de:8080/PSI2016/41
        x2 = datas[:,0]
        y1 = datas[:,3]
        y2 = datas[:,7]
        print 'Maximum Value =', round(max(y2)*100,1), '% at ', x2[np.argmax(y2)] , ' MeV'
        plotitsimple(x2,y1/1000.0,0.003*x2,0.02*y1/1000.0,'Energy [MeV]','Pi Stop Rate [kHz]','Range Curve',SavePath+'RangeCurve_155mm_2_Counts.pdf',plotlabel='Run 2')
        plotitsimple(x2,y2*100.0,0.003*x2,0.02*y2*100.0,'Energy [MeV]','Pi Stop Fraction [%]','Range Curve',SavePath+'RangeCurve_155mm_2_Fraction.pdf',plotlabel='Run 2')
        # For Normalized Plot (Normalized by taking Maximum and set it to 99%)
        # Uncomment the line below
        #plotitsimple(x,y2/y2.max()*0.99*100.0,0.002*x,0.02*y2/y2.max()*0.99*100.0,'Energy [MeV]','Pi Stop Fraction [%]','Normalized Range Curve (155 mm)','RangeCurve_155mm_Ratio_Normalized.pdf') 
        
    if i==3: # This run is from 4.9.2016, Morning Shift, ELOG Entry #32 : http://pomeron.physi.uni-heidelberg.de:8080/PSI2016/32
        x3 = datas[:,0]
        y3 = datas[:,1]
        print 'Maximum Value =', round(max(y3)*100,1), '% at ', x3[np.argmax(y3)] , ' MeV'
        plotitsimple(x3,y3*100.0,0.003*x3,0.01*y3*100.0,'Energy [MeV]','Pi Stop Fraction [%]','Range Curve',SavePath+'RangeCurve_155mm_1_Fraction.pdf',plotlabel='Run 1')
        # For Normalized Plot (Normalized by taking Maximum and set it to 99%)
        # Uncomment the line below
        #plotitsimple(x,y2/y2.max()*0.99*100.0,0.002*x,0.01*y2/y2.max()*0.99*100.0,'Energy [MeV]','Pi Stop Fraction [%]','Normalized Range Curve (155 mm)','RangeCurve_155mm_2_Ratio_Normalized.pdf') 
        
# Additional Plot:
        
pic = plt.figure(figsize=(PlotWidth,PlotHeight))    # Create a figure and call it pic
plt.errorbar(x3,y3*100.0/y3.max(),xerr=0.003*x3,yerr=0.02*y3*100.0/y3.max(),fmt='-s',color='b',label='Run 1, Max Value = ' + str(round(y3.max(),2)))              # Plot Data
plt.errorbar(x2,y2*100.0/y2.max(),xerr=0.003*x2,yerr=0.02*y2*100.0/y2.max(),fmt='-s',color='k',label='Run 2, Max Value = ' + str(round(y2.max(),2)))              # Plot Data
plt.title('Normalized Range Curves',y=1.05,fontsize=20)
plt.xlabel('Energy [MeV]',fontsize=18)
plt.ylabel('Pi Stop Fraction [%] (Normalized to max Value)',fontsize=18)
plt.xticks(fontsize=16)
plt.yticks(fontsize=16)
plt.legend(bbox_to_anchor=(1, 1), loc=1, borderaxespad=0.)
locs, labels = plt.xticks()
plt.xticks(np.arange(locs[0],locs[-1],(locs[1]-locs[0])/2.0))
plt.grid(True)                              # Turn on Grid
plt.savefig(SavePath+'RangeCurve_155mm_1_and_2_Fraction.pdf',bbox_inches='tight')   # Save Figure
plt.close()                                 # Close and Delete
del(pic)
    


print '----------------------'
print 'Done.'